# PASOS #

Cambiar la configuración del servidor en el archivo: application/config.js de igual manera en los momentos 2 y 3 dentro de la carpeta dashboard

```
#!javascript
var config = {
		NAMESPACE : 'upc.codeporta', // dejarlo tal como está
		APP : 'application/', // dejarlo tal como está
		URL_BASE : 'http://upc.doous/', // url de la aplicación
		URL_API : 'http://admision2.eastus2.cloudapp.azure.com/api/' // url del api
	};
```

Para el caso del identificador de la aplicación de facebook en el archivo: application/app.js
```
#!javascript
FacebookProvider.init('192579294473714');
```