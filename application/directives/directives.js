'use strict';

define(['config'], function (config) {
	var module = angular.module(config.NAMESPACE + '.directives',[]);
	return module;
});