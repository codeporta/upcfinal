'use strict';

define(['config','directives/directives'], function(configGlobal, module) {


    module.constant('validatorsConfig', {
        RUC: { reg: /^[0-9]{11}$/, val: 'uiruc' },
        EMAIL: { reg: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/, val: 'uiemail' },
        DNI: { reg: /^[0-9]{8}$/, val: 'uidni' },
        PASAPORTE: { reg: /^[0-9]{15}$/, val: 'uipasaporte' },
        CARNET: { reg: /^[0-9]{20}$/, val: 'uicarnet' },
        INTEGER: { reg: /^\-?\d*$/, val: 'uiinteger' },
        FLOAT: { reg: /^\-?\d+((\.|\,)\d+)?$/, val: 'uifloat' },
        UBIGEO: { reg: /^[0-9]{6}$/, val: 'uiubigeo' },
        PLAN: { reg: /^([0-9])+(\.)([0-9]){2}$/, val: 'uiplan' },
        DECIMAL: { reg: /^\-?\d+((\.)\d+)?$/, val: 'uidecimal' },
        BIRTHDATE : { reg: /^[0-9]{2}(\/)[0-9]{2}(\/)[0-9]{4}$/, val: 'uibirthdate' }
    });

    module.directive('uiValid', ['validatorsConfig', function(config) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {

                ctrl.$parsers.unshift(function(viewValue) {

                    if (config[attrs.uiValid].reg.test(viewValue)) {
                        ctrl.$setValidity(config[attrs.uiValid].val, true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity(config[attrs.uiValid].val, false);
                        return undefined;
                    }
                });

                ctrl.$formatters.unshift(function(viewValue) {

                    if (config[attrs.uiValid].reg.test(viewValue)) {
                        ctrl.$setValidity(config[attrs.uiValid].val, true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity(config[attrs.uiValid].val, false);
                        return viewValue;
                    }
                });

            }
        };
    }]);

 module.directive('numbersOnly', [function() {
        return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var count = parseInt(attrs.numbersOnly,10);
            modelCtrl.$parsers.push(function (inputValue) {                
                if(inputValue!==null){
                    if(inputValue.length<=count){
                        inputValue = inputValue;
                    }else{
                        inputValue = inputValue.substr(0,count) + 'x';
                    }
                }
                var transformedInput = inputValue ? inputValue.replace(/[^\d]/g,'') : null;
                if (transformedInput!=inputValue) {          
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                if(transformedInput!==null && transformedInput.length === count){
                    modelCtrl.$setValidity('uinumbersonly', true);
                }else{
                    modelCtrl.$setValidity('uinumbersonly', false);
                }
                return transformedInput;
            });
        }
    };
    }]);

module.directive('uiMap',function(){
    return {
        restrict:'EA',
        replace:true,
        scope:{
            uiConfig : '=',
            uiRender : '=',
            uiMarkers : '=',
            uiAddress : '=',
            uiRenderAddress : '=',
            uiRenderCreate : '='
        },
        link : function(scope,element,attrs){
            var ng = scope;            

            var maps = null;

            var fn = (function(){
                return {
                    reload : function(){

                        angular.forEach(ng.uiMarkers,function(val,key){
                            maps.marker({
                                position: val.latlong,
                                animation: google.maps.Animation.DROP,
                                icon: 'imagenes/public/marker.png'
                            });
                        });
                        
                    },
                    renderAddress : function(){
                        maps.latlng([
                            {address: ng.uiAddress}
                            ])
                        .then(function (results) {
                           maps.marker({
                            position: [results[0].lat(),results[0].lng()],
                            animation: google.maps.Animation.BOUNCE,
                            icon: 'imagenes/public/marker-mine.png'
                        });
                       });
                    }
                };
            })();
            ng.$watch('uiRender',function(newValue,oldValue){
                if(typeof newValue!=='undefined' && newValue){
                    fn.reload();
                    ng.uiRender = false;
                }
            });
            ng.$watch('uiRenderAddress',function(newValue,oldValue){
                if(typeof newValue!=='undefined' && newValue){
                    fn.renderAddress();
                    ng.uiRenderAddress = false;
                }
            });

            ng.$watch('uiRenderCreate',function(newValue,oldValue){
                if(typeof newValue!=='undefined' && newValue){
                    maps = $(element).gmap3({
                        center : ng.uiConfig.center,
                        zoom :  ng.uiConfig.zoom
                    });
                }
            });

        },
        template : '<div></div>'
    };
});

module.directive('casoExitoVideo',function(){
    return{
        restrict :'EA',
        replace:true,
        scope:{
            uiUrl : '=',
            uiThumbnail :'='
        },
        link:function(scope,element,attrs){
            $(element).lightGallery({
                width: '700px',
                height: '470px',
                mode: 'lg-fade',
                counter: false,
                hideBarsDelay:1000*60*60,
                closable: false,
                download: false,
                zoom: false,
                addClass: 'fixed-size'
            });
        },
        template : '<div>'+
        '<a class="caso-exito-video" href="{{uiUrl}}" style="display:block;border:none;opacity:0.50;">'+
        '<img data-ng-src="{{uiThumbnail}}" class="w-100">'+
        '</a>'+
        '</div>'
    };
});

module.directive('casoExito',function(){
    return{
        restrict :'EA',
        replace:true,
        scope:{
            uiConfig : '='
        },
        link:function(scope,element,attrs){
            $(element).lightGallery({
                width: '700px',
                height: '470px',
                mode: 'lg-fade',
                counter: false,
                hideBarsDelay:1000*60*60,
                closable: false,
                download: false,
                zoom: false,
                addClass: 'fixed-size'
            });
        },
        template : '<div>'+
        '<a href="{{uiConfig.url}}" style="display:block;border:none;opacity:0.50;" data-sub-html="<h4>{{uiConfig.nombre}}</h4><p>{{uiConfig.carrera}}</p>">'+
        '<img data-ng-src="{{uiConfig.imagen}}" class="padding-image ">'+
        '</a>'+
        '</div>'
    };
});
module.directive('uiMapaConvenios',function(){
    return{
        restrict :'EA',
        replace:true,
        scope:{
            uiSelectMarker : '&',
            uiSelectRegion : '&',
            uiRender : '=',
            uiMarkers:'='
        },
        link:function(scope,element,attrs){
            var id = scope.$id + new Date().getTime();
            $(element).attr('id',id);
            var x = $(element).vectorMap({
                map: 'world_mill',
                backgroundColor: '#ffffff',
                regionStyle : {
  initial: {
    fill: '#cccccc',
    "fill-opacity": 1,
    stroke: 'none',
    "stroke-width": 0,
    "stroke-opacity": 1
  },
  hover: {
    "fill-opacity": 0.8,
    cursor: 'pointer'
  },
  selected: {
    fill: 'yellow'
  }
},
markerStyle : {
  initial: {
    fill: 'red',
    stroke: '#E50505',
    "fill-opacity": 1,
    "stroke-width": 7,
    "stroke-opacity": 0.5,
    r: 6
  },
  hover: {
    stroke: '#E50505',
    "stroke-width": 9,
    cursor: 'pointer',
    "fill-opacity": 0.4,
  },
  selected: {
    fill: 'blue'
  },
  selectedHover: {
  }
},
                onRegionClick : function(e,code){                    
                    scope.$apply(function(){
                        scope.uiSelectRegion({code:code});
                    });
                },
                onMarkerClick : function(e,code){
                    scope.$apply(function(){
                        scope.uiSelectMarker({code:code});
                    });
                }
            });

                scope.$watch('uiRender',function(newValue,oldValue){
                    if(typeof newValue!=='undefined' && newValue){
                        var x = $(element).vectorMap('get', 'mapObject');
                        x.removeAllMarkers();
                        x.addMarkers(scope.uiMarkers);
                        scope.uiRender = false;
                    }
                });

        }
    };
});
module.directive('uiSlider',function(){
    return{
        restrict :'EA',
        replace:true,
        scope:{
            uiPrev:'=',
            uiNext:'=',
            uiRender:'=',
            uiReset:'='
        },
        link:function(scope,element,attrs){
          
            var owl = $(element);

                scope.$watch('uiPrev',function(newValue,oldValue){
                    if(typeof newValue!=='undefined' && newValue){
                        owl.trigger('owl.prev');
                        scope.uiPrev = false;
                    }
                });

                scope.$watch('uiNext',function(newValue,oldValue){
                    if(typeof newValue!=='undefined' && newValue){
                        owl.trigger('owl.next');
                        scope.uiNext = false;
                    }
                });
                scope.$watch('uiRender',function(newValue,oldValue){
                    if(typeof newValue!=='undefined' && newValue){
                        var opt = {
                                navigation : false,
                                singleItem: (attrs.uiSlider!=='') ? false : true,
                                pagination:false                            
                            };
                            if(typeof attrs.uiSlider!=='undefined'){
                                opt.items = parseInt(attrs.uiSlider);
                            }

                        var owlTemp = $(element).data('owlCarousel');
                        //console.log('oldTemp: ', owlTemp);
                        if(typeof owlTemp!=='undefined'){
                            //console.log('typeof--------: ',typeof owlTemp.destroy);
                           //owlTemp.destroy();
                            $(element).data('owlCarousel').reinit();
                            owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
                           //owl.trigger('destroy.owl.carousel');
                           //owl.trigger('refresh.owl.carousel');
                           //owl = owl.owlCarousel(opt);

                        }else{
                            //console.log('owl: ',owl);
                            owl = owl.owlCarousel(opt);
                        }
                        scope.uiRender = false;
                    }
                });

                scope.$watch('uiReset',function(newValue,oldValue){
                    if(newValue!=='' && newValue){
                        var owl = $(element).data('owlCarousel');
                        owl.destroy();
                        scope.uiReset = false;
                    }
                });

        }
    };
});

module.directive('uiMouseover',function(){
    return {
        restrict : 'EA',
        scope : {
            uiDescription : '='
        },
        link : function(scope,element,attrs){
            scope.uiDescription = null;
            $(element).mouseover(function(){
                scope.$apply(function(){
                    scope.uiDescription = attrs.uiMouseover;
                });
            });
            /*$(element).mouseout(function(){
                scope.$apply(function(){
                    scope.uiDescription = null;
                });
            });*/
        }
    }
});

module.directive('uiFooter',function(){
    return {
        restrict : 'EA',
        replace : true,
        templateUrl: configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/footer.html'
    }
});

module.directive('uiHeader',function(){
    return {
        restrict : 'EA',
        replace : true,
        templateUrl: configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/header.html'
    }
});

module.directive('uiOnlyHeader',function(){
    return {
        restrict : 'EA',
        replace : true,
        templateUrl: configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/only-header.html'
    }
});

module.directive('uiClick',function($timeout){
    return {
        restrict : 'A',
        scope :{
            uiPos : '=',
            uiDimensions : '='
        },
        link:function(scope,element,attrs){

            if(attrs.uiClick===''){
                $(element).click(function(e){
                    e.preventDefault();

                var that = $(this);
                

                $timeout(function(){
                    var cont = 0;
                    angular.forEach(scope.uiDimensions,function(val,key){
                        if(typeof val.selected!=='undefined' && val.selected){
                            cont++;
                        }
                    });
                    if(cont < 4){
                            //$(that).fadeOut('fast');
                                scope.$apply(function(){
                                    scope.uiDimensions[scope.uiPos].selected = true;
                                });
                        }

                },50);
                
                
                });
            }else{
                $(element).click(function(e){
                    e.preventDefault();

                var that = $(this);

                $timeout(function(){
                        //$(that).fadeOut('fast');
                        scope.$apply(function(){
                            scope.uiDimensions[scope.uiPos].selected = false;
                        });
                },50);
                
                
                });
            }

            
        }
    }
});



module.directive('uiFraseAleatoria',function(){
    return {
        restrict : 'A',
        scope :{
            uiRender : '='
        },
        link:function(scope,element,attrs){

            scope.$watch('uiRender',function(newValue,oldValue){
                    if(typeof newValue!=='undefined' && newValue){
   
                        $(element).owlCarousel({
                                navigation : false,
                                singleItem : true,
                                pagination : false,
                                autoPlay : true
                            });
                        scope.uiRender = false;
                    }
                });
        }
    }
});

module.directive('uiResize',function(){
    return {
        restrict : 'A',
        scope :{
            uiRender : '='
        },
        link:function(scope,element,attrs){

            scope.$watch('uiRender',function(newValue,oldValue){                
                    if(typeof newValue!=='undefined'){
                            if(newValue){
                                $(element).parent().show();
                                $(element).animate({
                                    'width' : '100%'
                                },'fast',function(){
                                    $(document).trigger('ANIMATE-EVENT-END-DIRECTIVE');
                                });
                                
                            }else{
                                $(element).animate({
                                    'width' : '0%'
                                },'fast',function(){
                                   $(element).parent().hide();                                   
                                });
                                
                            }
                    }else{
                        $(element).parent().hide();
                    }
                });
        }
    }
});

module.directive('uiFade',function(){
    return {
        restrict : 'A',
        scope :{
            uiRender : '='
        },
        link:function(scope,element,attrs){

            scope.$watch('uiRender',function(newValue,oldValue){                
                    if(typeof newValue!=='undefined'){
                            if(newValue){
                                $(element).hide();
                            }else{
                                $(element).fadeIn('fast');
                            }
                    }else{
                        $(element).show();
                    }
                });
        }
    }
});

module.directive('uiFadeOther',function(){
    return {
        restrict : 'A',
        scope :{
            uiRender : '='
        },
        link:function(scope,element,attrs){

            scope.$watch('uiRender',function(newValue,oldValue){                
                    if(typeof newValue!=='undefined'){
                            if(newValue){
                                $(element).fadeIn('fast');
                            }else{
                                $(element).hide();
                            }
                    }else{
                        $(element).hide();
                    }
                });
        }
    }
});

module.directive('uiHeaderCampus',function($http){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '='
        },
        link:function(scope,element,attrs){

            var isOpen = false;

            $(element).hide();

            (function(){
                $http.get('json/campus.json').then(function(response){
                    scope.campusList = response.data;
                  },function(error){

                  });
            })();

            scope.$watch('uiRender',function(newValue,oldValue){                
                    if(typeof newValue!=='undefined' && newValue){
                        console.log(newValue,isOpen);
                            if(isOpen){
                                isOpen = false;
                                $(element).slideUp('fast');
                            }else{
                                isOpen = true;
                                $(element).slideDown('fast');
                            }
                            scope.uiRender = false;
                    }
                });
            
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/header-campus.html'
    }
});


module.directive('uiIconButton',function(){
    return {
        restrict : 'EA',
        scope :{
            uiRender : '=',
            //uiCloseAll : '=',
            //uiClose : '=',
            uiOpen :'='
        },
        link :function(scope,element,attrs){

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            if(attrs.uiIconButton.length===0){
                $(element).html('<img class="button-small" src="./imagenes/button-down-2.png" alt="">');
            }

            

            //var isOpen = false;

            $(element).click(function(e,d){
                e.preventDefault();
                if(scope.uiOpen){
                    //isOpen = false;
                    scope.safeApply(function(){
                        scope.uiRender = false;
                    });
                }else{
                    //isOpen = true;
                    scope.safeApply(function(){
                        scope.uiRender = true;
                    });
                }
               /* console.log(isOpen,d);
                if(isOpen){
                    isOpen = false;
                    scope.safeApply(function(){
                        scope.uiRender = false;
                       // scope.uiOpen = false;
                    });
                    $(element).find('img').remove();
                    $(element).html('<img class="button-small" src="./imagenes/button-down-2.png" alt="">');
                }else{
                    if(typeof d === 'undefined'){
                       console.info('open');
                        isOpen = true;
                        scope.safeApply(function(){
                            scope.uiRender = true;
                            scope.uiCloseAll = true;
                            //scope.uiOpen = true;
                        });
                        $(element).find('img').remove();
                        $(element).html('<img class="button-small" src="./imagenes/button-up-2.png" alt="">'); 
                    }                    
                }*/
                //console.log(isOpen);
            });

            /*scope.$watch('uiClose',function(newValue,oldValue){ 
                if(typeof newValue!=='undefined' && newValue){                    
                    $(element).trigger('click',{data:true});
                    scope.uiClose = false;
                }
            });*/
            if(attrs.uiIconButton.length===0){
                scope.$watch('uiOpen',function(newValue,oldValue){ 
                    if(typeof newValue!=='undefined'){                    
                        if(!newValue){
                            $(element).find('img').remove();
                            $(element).html('<img class="button-small" src="./imagenes/button-down-2.png" alt="">').hide().fadeIn();
                        }else{
                            $(element).find('img').remove();
                            $(element).html('<img class="button-small" src="./imagenes/button-up-2.png" alt="">').hide().fadeIn();
                        }
                    }
                });
            }
        }
    };
});

module.directive('uiFacultadesDesktop',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '=',
            uiItems : '=',
            uiOpen : '='
        },
        link:function(scope,element,attrs){
            var ng = scope;
            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            $(element).hide();
            scope.uiOpen = false;
            var isRender = false;

            var doFacultadesCopia = function(){
                var clone = angular.copy(ng.uiItems);
                clone.splice(0,1);
                  ng.facultadesIteratorDesktop = [];
                  var pagesDesktop = clone.length / 10;
                  pagesDesktop = Math.floor(1 + pagesDesktop);

                  var cont = 0;
                  for (var i = 0; i < pagesDesktop; i++) {
                    ng.facultadesIteratorDesktop[i] = [];
                    for (var k = 0; k < 2; k++) {
                      var temp = [];
                      for (var j = 0; j < 5; j++) {
                        if (typeof clone[cont] !== 'undefined') {
                          temp.push(clone[cont]);
                          cont++;
                        }
                      }
                      ng.facultadesIteratorDesktop[i][k] = temp;
                    }
                  }
                  isRender = true;
            };

            scope.$watch('uiRender',function(newValue,oldValue){                
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        $(element).slideDown('fast',function(){
                            scope.safeApply(function(){
                                if(!isRender){
                                    doFacultadesCopia();
                                }
                                scope.renderSliderDesktop = true;
                                scope.uiOpen = true;
                            });
                        });
                    }else{
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/facultades-desktop.html'
    }
});


module.directive('uiFacultadesMobile',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '=',
            uiItems : '=',
            uiOpen : '='
        },
        link:function(scope,element,attrs){
            var ng = scope;
            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            $(element).hide();
            scope.uiOpen = false;
            var isRender = false;

            var doFacultadesCopia = function(){
                var clone = angular.copy(ng.uiItems);
                clone.splice(0,1);
                  ng.facultadesIteratorDesktop = [];
                  var pagesDesktop = clone.length / 4;
                  pagesDesktop = Math.floor(1 + pagesDesktop);

                  var cont = 0;
                  for (var i = 0; i < pagesDesktop; i++) {
                    ng.facultadesIteratorDesktop[i] = [];
                    for (var k = 0; k < 2; k++) {
                      var temp = [];
                      for (var j = 0; j < 2; j++) {
                        if (typeof clone[cont] !== 'undefined') {
                          temp.push(clone[cont]);
                          cont++;
                        }
                      }
                      ng.facultadesIteratorDesktop[i][k] = temp;
                    }
                  }
                  isRender = true;
            };

            scope.$watch('uiRender',function(newValue,oldValue){                
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        $(element).slideDown('fast',function(){
                            scope.safeApply(function(){
                                if(!isRender){
                                    doFacultadesCopia();
                                }
                                scope.renderSliderMobile = true;
                                scope.uiOpen = true;
                            });
                        });
                    }else{
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/facultades-mobile.html'
    }
});

module.directive('uiCampusDesktop',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '=',
            uiItems : '=',
            uiOpen : '='
        },
        link:function(scope,element,attrs){

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            $(element).hide();
            scope.uiOpen = false;

            scope.$watch('uiRender',function(newValue,oldValue){                
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        $(element).slideDown('fast',function(){
                           scope.safeApply(function(){
                                scope.uiOpen = true;
                            });
                        });
                    }else{
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/campus-desktop.html'
    }
});

module.directive('uiCampusMobile',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '=',
            uiItems : '=',
            uiOpen : '='
        },
        link:function(scope,element,attrs){        

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            $(element).hide();
            scope.uiOpen = false;

            scope.$watch('uiRender',function(newValue,oldValue){                
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        $(element).slideDown('fast',function(){
                           scope.safeApply(function(){
                                scope.uiOpen = true;
                            });
                        });
                    }else{
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/campus-mobile.html'
    }
}); 

module.directive('uiCampusMobileOne',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope :{
            uiRender : '=',
            uiItems : '=',
            uiOpen : '='
        },
        link:function(scope,element,attrs){        

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            $(element).hide();
            scope.uiOpen = false;

            scope.$watch('uiRender',function(newValue,oldValue){                
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        $(element).slideDown('fast',function(){
                           scope.safeApply(function(){
                                scope.uiOpen = true;
                            });
                        });
                    }else{
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/campus-mobile-one.html'
    }
}); 

module.directive('uiNight',function(){
    return {
        restrict : 'A',
        link:function(scope,element,attrs){
            var day = new Date().getHours();
            if(day >= 4 && day <= 12){
                $(element).css({
                    'background-image': 'url(imagenes/manana.png)'
                });
            }else if(day > 12 && day <= 18){
                $(element).css({
                    'background-image': 'url(imagenes/tarde.png)'
                });
            }else{
                $(element).css({
                    'background-image': 'url(imagenes/noche.png)'
                });    
            }
        }
    }
});


module.directive('uiCounter',function($http){
    return {
        restrict : 'A',
        link:function(scope,element,attrs){
            $(element).hide();
            (function(){
                $http.get(configGlobal.URL_API + 'applicant/period/').then(function(response){
                    if(response.status===200){
                        $(element).html(response.data.remaining_days);
                        $(element).fadeIn('fast');    
                    }
                },function(error){

                });

                

            })();
        }
    }
});

module.directive('uiScroll',function($http){
    return {
        restrict : 'A',
        link:function(scope,element,attrs){
            $(element).height(100);
            $(element).addClass('scroll-nice');
            $(element).enscroll({
                  showOnHover: true,
                  verticalScrolling: true,
                  verticalTrackClass: 'track3',
                  verticalHandleClass: 'handle3'
                });
        }
    }
});

module.directive('uiConvenio', function($http, $timeout){
    return {
        restrict : 'EA',
        replace : true,
        scope : {
            uiFacultades : '=',
            uiRender : '=',
            uiConvenios : '=',
            uiRenderConvenios : '=',
            uiDetail :'=',
            uiAnimation : '=',
            uiOpen : '='
        },
        link : function(scope, element, attrs){
            var ng = scope;

             scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };


            if(typeof attrs.uiOpen!=='undefined'){
                $(element).hide();            
                scope.uiOpen = false;
            }        

            ng.models = (function(){
                return {
                    facultad : null,
                    facultades : [],
                    facultadesSeleccionadas : [],
                    facultadesRecomendadas : [],
                    markers : []
                };
            })();

            ng.api = (function(){
                return {
                    getConvenios: function (idFacultad, isMobile) {
                      ng.models.markers = [];
                      APP.DATA.FN.showLoadingPage('Cargando...',true);
                      console.log('idFacultad: ',idFacultad);
                      $http.get('json/convenios/'+idFacultad+'.json').then(function(response){
                        ng.models.markers = response.data;
                        if(isMobile){
                          $('#modal-convenio-mobile').modal('show');
                          $timeout(function(){
                            fn.doConveniosMobile();
                          },200);
                        }else{
                          ng.renderMarkers = true;
                        }            
                        $timeout(function(){
                          APP.DATA.FN.removeLoadingPage();
                        },500);
                      },function(error){
                        if(error.status===404){
                            APP.DATA.FN.error('No existen convenios...');
                            ng.models.markers = [];
                            if(isMobile){
                              $('#modal-convenio-mobile').modal('show');
                              $timeout(function(){
                                fn.doConveniosMobile();
                              },200);
                            }else{
                              ng.renderMarkers = true;
                            }            
                            $timeout(function(){
                              APP.DATA.FN.removeLoadingPage();
                            },500);
                        }                        
                      });
                      
                    }
                };
            })();
            var fn = (function(){
                return {
                    doConveniosMobile : function(){
                          ng.uiConvenios = [];
                          ng.uiRenderConvenios = true;
                          var pages = ng.models.markers.length / 4;
                          pages = Math.floor(1 + pages);

                          var cont = 0;
                          for (var i = 0; i < pages; i++) {
                            ng.uiConvenios[i] = [];
                            for (var k = 0; k < 2; k++) {
                              var temp = [];
                              for (var j = 0; j < 2; j++) {
                                if (typeof ng.models.markers[cont] !== 'undefined') {
                                  temp.push(ng.models.markers[cont]);
                                  cont++;
                                }
                              }
                              ng.uiConvenios[i][k] = temp;
                            }
                          }
                          ng.uiRenderConvenios = true;
                        },
                        getMarker : function(code){
        
                          ng.uiDetail = ng.models.markers[code];

                          $('#modalConveniosDesktop').modal('show');

                        },
                };
            })();
            ng.onClick = (function(){
                return {
                    modal : function(row){
                      ng.api.getConvenios(row.id, true);
                    },
                    back : function(){
                        ng.models.facultad = ng.models.facultades[0]
                        ng.models.markers = [];
                        ng.renderMarkers = true;
                    },
                    renderMarkers : function(row){
                        ng.api.getConvenios(row.id, false);
                    },
                    selectedMap: function (code) {
                      fn.getMarker(code);
                    }
                };
            })();
            ng.onChange = (function(){
                return {
                    facultad : function(){
                        ng.models.facultadesRecomendadas = [];
                        console.log('ng.models.facultadesRecomendadas: ',angular.copy(ng.models.facultadesRecomendadas));

                                    var exits = false;
                                    for(var i =0; i<ng.models.facultadesSeleccionadas.length;i++){
                                        if(ng.models.facultadesSeleccionadas[i].id === ng.models.facultad.id){
                                            exits = true;
                                            break;
                                        }
                                    }
                                    if(!exits){
                                        ng.models.facultadesSeleccionadas.push(angular.copy(ng.models.facultad));
                                    }

                                    console.log(angular.copy(ng.models.facultadesSeleccionadas));

                                    for(var j =0; j<ng.models.facultades.length;j++){
                                        for(var i =0; i<ng.models.facultadesSeleccionadas.length;i++){
                                            if(ng.models.facultades[j].id === ng.models.facultadesSeleccionadas[i].id || ng.models.facultades[j].id===-1 ){
                                            console.info(ng.models.facultades[j].id , ng.models.facultadesSeleccionadas[i].id);      
                                                break;
                                            }else{
                                                 //ng.models.facultadesRecomendadas.push(angular.copy(ng.models.facultades[j]));
                                                /*var temp = false;
                                                for(var x =0; x < ng.models.facultadesRecomendadas.length;x++){
                                                    if(ng.models.facultadesRecomendadas[x].id === ng.models.facultadesSeleccionadas[i].id){
                                                        temp = true;
                                                        break;
                                                    }
                                                }
                                                if(!temp){
                                                    ng.models.facultadesRecomendadas.push(ng.models.facultades[j]);
                                                }*/
                                                console.info(angular.copy(ng.models.facultades[j]));
                                                ng.models.facultadesRecomendadas.push(ng.models.facultades[j]);
                                                break;
                                            }
                                        }
                                    }
                                    
                                    ng.api.getConvenios(ng.models.facultad.id,false);
                    }
                };
            })();
            ng.$watch('uiRender',function(newValue, oldValue){
                if(typeof newValue!=='undefined' && newValue){
                    ng.models.facultades = angular.copy(ng.uiFacultades);                    
                    ng.models.facultades.unshift({
                        nombre: 'Seleccione...',
                        id: -1
                    });
                    ng.models.facultad = ng.models.facultades[0];
                    ng.uiRender = false;
                }
            });

            scope.$watch('uiAnimation',function(newValue,oldValue){
            console.info(newValue,oldValue);               
                if(typeof newValue!=='undefined'){
                    if(newValue){
                        console.log($(element));
                        $(element).slideDown('fast',function(){
                           scope.safeApply(function(){
                                scope.uiOpen = true;
                            });
                           $(window).resize();
                        });
                    }else{
                        console.log(':o');
                        $(element).slideUp('fast',function(){
                            scope.safeApply(function(){
                                scope.uiOpen = false;
                            });
                        });
                    }
                }
            });

        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/convenio.html'
    };
});


module.directive('uiModalConvenioMobile',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope : {
            uiConvenios : '=',
            uiRender : '='
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/modal-convenio-mobile.html'
    };
});

module.directive('uiModalConvenio',function(){
    return {
        restrict : 'EA',
        replace : true,
        scope : {
            uiDetail : '='
        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/modal-convenio.html'
    };
});

module.directive('uiEvento',function($timeout,$http,globalService,$location){
    return {
        restrict : 'EA',
        replace : true,
        scope : {
            
        },
        link : function(scope,element,attrs){
            var ng= scope;
            ng.models = (function(){
                return {
                    eventsList : [],
                    eventsListMobile : []
                };
            })();
            var fn = (function(){
                return {
                    getHours : function(date){
                                    var d = new Date(date);
                                    var hour = d.getHours();
                                    var minutes = d.getMinutes();
                                    var am = 'AM';
                                    if(hour < 10){
                                        hour = '0'+hour;
                                    }
                                    if(minutes < 10){
                                        minutes = '0'+minutes;
                                    }
                                    return hour + ':' + minutes + ' h.';
                                }
                };
            })();
            ng.api = (function(){
                return {
                    getEvents : function(){
                        $http.get(configGlobal.URL_API + 'events/available-events/').then(function (response) {
                            if (response.status === 200) {
                                ng.models.eventsList = [];
                                ng.models.eventsListMobile = [];
                                var events = [];
                                angular.forEach(response.data,function(val,key){
                                    angular.forEach(val.events,function(v,k){
                                        v.starts_at_parser = fn.getHours(v.starts_at);
                                        v.ends_at_parser = fn.getHours(v.ends_at);
                                        v.date = val.date;
                                        events.push(v);
                                    });
                                });
                                var cont = 0;
                                var temp = [];
                                ng.models.eventsListMobile = angular.copy(events);
                                angular.forEach(events,function(val,key){
                                    if(cont < 3){
                                        temp.push(val);
                                        cont++;
                                        if(cont===response.data.length){
                                            ng.models.eventsList.push(angular.copy(temp));
                                        }
                                    }else{
                                        ng.models.eventsList.push(angular.copy(temp));
                                        temp = [];
                                        temp.push(val);
                                        cont = 1;
                                    }
                                });
                                
                                ng.renderSliderEventosMobile = true;

                            }
                        }, function (error) {

                        });
                    }
                };
            })();

            ng.onClick = (function(){
                return {
                    registerEvent : function(r){
                                    globalService.evento = r;
                                    $location.path('registro-evento');
                                }
                };
            })();

            $(document).on('ANIMATE-EVENT-END-DIRECTIVE', function () {
                $timeout(function () {
                    ng.renderSliderEventos = true;
                }, 20);
            });
            
            ng.api.getEvents();

        },
        templateUrl : configGlobal.URL_BASE + configGlobal.APP + 'templates/directives/evento.html'
    };
});



});