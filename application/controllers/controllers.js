'use strict';

define(['config'], function (config) {
	var module = angular.module(config.NAMESPACE + '.controllers',[]);
	
	module.config(['$controllerProvider','$compileProvider','$filterProvider','$provide', function($controllerProvider, $compileProvider,$filterProvider,$provide){

			module.register = {
				controller: $controllerProvider.register,
			    directive: $compileProvider.directive,
			    filter: $filterProvider.register,
			    factory: $provide.factory,
			    service: $provide.service			
			};

		}
	]);

	return module;
});