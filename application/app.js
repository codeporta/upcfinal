'use strict';

define(['config', 'angular-route', 'angular-cookies', 'controllers/controllers', 'modules/routeResolver'], function (config) {

  var app = angular.module('appModule', ['ngRoute', 'ngCookies','facebook', 'routeResolverServices' ,config.NAMESPACE + '.controllers']);

  app.constant('constants', {

  });
  app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$httpProvider', '$provide', 'FacebookProvider', 
    function ($routeProvider, routeResolverProvider, $controllerProvider, $compileProvider, $filterProvider, $httpProvider, $provide, FacebookProvider) {

      $compileProvider.debugInfoEnabled(false);

      FacebookProvider.init('192579294473714');

      app.register =
        {
          controller: $controllerProvider.register,
          directive: $compileProvider.directive,
          filter: $filterProvider.register,
          factory: $provide.factory,
          service: $provide.service
        };

      /*$httpProvider.defaults.headers.post['Accept'] = 'application/json';
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';

     $httpProvider.interceptors.push(function($q, $filter) {
        return {
          'request': function(conf) {
            if(conf.url.search('.html')===-1 && conf.url.search('templateIframeId')===-1){
              conf.url = config.URL_API + conf.url;
              conf.headers.Authorization = config.TOKEN;              
            }
            return conf;
          },
          'responseError': function(rejection) {
            if(rejection.status === 401) {
              console.log('No autorizado');
            }
            return $q.reject(rejection);
          }
        };
      });*/

      var route = routeResolverProvider.route;
      /* if(APP.DATA.CONFIG.ALIAS_ROUTES_LIST.length > 0){
         $.each(APP.DATA.CONFIG.ALIAS_ROUTES_LIST, function (key, val) {
           var aliashash= val.split('/');
           var aliasRoute = '';
           for (var i = 0; i < aliashash.length; i++) {
             aliasRoute += '/' + aliashash[i];
           }
           $routeProvider.when(aliasRoute, route.resolve(aliashash[0]));
           //console.log("route: ", aliasRoute);
         });
       }*/
      $routeProvider.when('/404', route.resolve('404'));
      $routeProvider.when('/inicio', route.resolve('inicio'));
      $routeProvider.when('/carrera/:uri', route.resolve('carrera'));
      $routeProvider.when('/facultad/:uri', route.resolve('facultad'));
      $routeProvider.when('/campus/:uri', route.resolve('campus'));
      $routeProvider.when('/acreditaciones', route.resolve('acreditaciones'));
      $routeProvider.when('/aptitud', route.resolve('aptitud'));
      $routeProvider.when('/testimonios', route.resolve('testimonios'));
      $routeProvider.when('/vida-universitaria', route.resolve('vida-universitaria'));
      $routeProvider.when('/deportes', route.resolve('deportes'));
      $routeProvider.when('/registro-evento', route.resolve('registro-evento'));
      $routeProvider.when('/login', route.resolve('login'));
      $routeProvider.when('/registro', route.resolve('registro'));
      $routeProvider.when('/postula', route.resolve('postula'));
      $routeProvider.when('/interno', route.resolve('interno'));
      $routeProvider.when('/', { redirectTo: '/inicio' });
      $routeProvider.otherwise({ redirectTo: '/404' });

    }]);


  app.controller('appController', function ($scope, $rootScope, $location, $http, globalService) {

    var ng = $scope;


    $rootScope.$on('$routeChangeStart', function (event, next, current) {
      //console.log('change: ', event);
      APP.DATA.FN.showLoadingPage('Cargando...',true);
    });

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
      //console.log('completo: ', event);
      $("html, body").stop().animate({ scrollTop: 0 }, '500', 'swing');
    });

    $rootScope.$on("$routeChangeError", function (event) {
      console.log('error: ', event);
      APP.DATA.FN.removeLoadingPage();
      $location.path('/404');
    });


  });

  return app;
});
