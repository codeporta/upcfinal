'use strict'

define([], function(){

	var config = {
		NAMESPACE : 'upc.codeporta',
		APP : 'application/',
		URL_BASE : 'http://upc.doous/',
		URL_API : 'http://admision2.eastus2.cloudapp.azure.com/api/'
	};

	return config;
});