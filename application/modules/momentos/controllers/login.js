'use strict';

define(['config', 'modules/momentos/init'], function (config, module) {

  var controller = module.name + '.login';

  module.register.controller(controller, ['$scope', '$http', 'globalService', '$timeout', 'Facebook', '$location', '$cookies', function ($scope, $http, globalService, $timeout, Facebook, $location, $cookies) {

    var ng = $scope;

    if (typeof $cookies.get('AUTHENTICATE') !== 'undefined' && typeof $cookies.get('AUTHENTICATE') !== null && $cookies.get('AUTHENTICATE')) {

      $location.path('interno');
    } else {


      ng.models = (function () {
        return {
          auth: null,
          user: null,
          login: {
            email: null,
            password: null
          }
        };
      })();

      ng.api = (function () {
        return {
          register: function () {
            var data = {
              email : ng.models.login.email,
              password :  ng.models.login.password
            };
            APP.DATA.FN.showLoadingPage('Registrando usuario...',true);
            $http.post(config.URL_API + 'auth/register-by-email/', data).then(function (response) {
              APP.DATA.FN.removeLoadingPage();
              if(response.status===200){
                globalService.auth_email = response.data;
                $location.path('registro');
              }
            }, function (error) {
              APP.DATA.FN.removeLoadingPage();
              if(typeof error.data.email!=='undefined'){
                APP.DATA.FN.error(error.data.email[0]);
              }else{
                if(error.data==='email_already_registered'){
                  APP.DATA.FN.error('El correo ya está registrado, inicie sesión...');
                }else{
                  APP.DATA.FN.error('Inténtelo otra vez...');
                }                
              }
            });
          },
          registerFacebook: function (token) {
            var data = {
              backend: 'facebook',
              access_token: token
            };
            $http.post(config.URL_API + 'auth/register-by-token/facebook/', data).then(function (response) {
              if (response.status === 200) {                
                ng.api.getUser(response.data);
              }              
            }, function (error) {
              APP.DATA.FN.removeLoadingPage();
              APP.DATA.FN.error('Ocurrió un error');
            });
          },
          getDataFacebook: function () {

            Facebook.getLoginStatus(function (response) {
              if (response.status === 'connected') {

                Facebook.api('/me?fields=id,first_name,gender,middle_name,last_name,email,location,locale,permissions', function (response) {
                  if (typeof response.error !== 'undefined') {
                    APP.DATA.FN.error('Inténtelo otra vez...');                    
                    APP.DATA.FN.removeLoadingPage();
                  } else {               
                    ng.models.user = response;
                    ng.api.registerFacebook(ng.models.auth.accessToken);
                  }
                });
              }else{
                APP.DATA.FN.removeLoadingPage();
                APP.DATA.FN.error('Error con facebook...');
              }
            });
          },
          getUser : function(data){
            var cf =  {
              headers : {
                'Authorization': 'Token ' + data.token
              }
            };
            $http.get(config.URL_API + 'applicant/',cf).then(function(response){
                APP.DATA.FN.removeLoadingPage();
                if(response.status===200){
                  if(response.data.m1_form_complete){
                    $cookies.put('AUTHENTICATE', true);
                    $cookies.put('TOKEN', data.token);
                    $cookies.put('NAME', response.data.first_name);
                    $cookies.put('TYPE', response.data.level);
                    switch(response.data.status_id){
                      case 10:
                        $location.path('interno');
                        $cookies.put('MOMENTO', 1);
                      break;
                      case 11:
                        window.location.href = 'dashboard/#/momento2';
                        $cookies.put('MOMENTO', 2);
                      break;
                      case 12:
                        window.location.href = 'dashboard/#/momento3';
                        $cookies.put('MOMENTO', 3);
                      break;
                    }                    
                  }else{
                    globalService.auth_email = data;                   
                    $location.path('registro');
                  }
                }
            },function(error){
              APP.DATA.FN.removeLoadingPage();
              APP.DATA.FN.error('Intentelo otra vez...');
            });
          },
          login : function(){
            var data = {
              email : ng.models.login.email,
              password :  ng.models.login.password
            };
            APP.DATA.FN.showLoadingPage('Iniciando sesión...',true);
            $http.post(config.URL_API + 'auth/login-by-email/',data).then(function(response){              
              if(response.status===200){                
                ng.api.getUser(response.data);
              }
            },function(error){
              APP.DATA.FN.removeLoadingPage();
              if(typeof error.data.email!=='undefined'){
                APP.DATA.FN.error(error.data.email[0]);
              }else{
                if(error.status===401){
                  APP.DATA.FN.error('Datos incorrectos...');
                }else{
                  APP.DATA.FN.error('Intentelo otra vez...');
                }
              }
            });
          }

        };
      })();

      ng.onClick = (function () {
        return {
          send: function (frm) {
            if (frm.$valid) {
              APP.DATA.FN.showLoadingPage('Registrando...');
              ng.models.isSent = true;
            }
          },
          facebook: function () {
            APP.DATA.FN.showLoadingPage('Conectando con facebook...', true);
            Facebook.getLoginStatus(function (response) {
              if (response.status === 'connected') {
                ng.models.auth = response.authResponse;
                ng.api.getDataFacebook();

              } else {
                Facebook.login(function (response) {
                  if (response.status === 'connected') {
                    ng.models.auth = response.authResponse;
                    ng.api.getDataFacebook();
                  }else{
                    APP.DATA.FN.removeLoadingPage();
                  }

                }, { scope: 'public_profile,email' });
              }
            });
          },
          login: function (isValid) {
            if (isValid) {
              ng.api.login();
            }
          },
          register : function(isValid){
            if(isValid){
              ng.api.register();
            }
          }
        }
      })();


      $timeout(function () {
        APP.DATA.FN.removeLoadingPage();
      }, 1200);

    }

  }]);

});