'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.postula';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout','$cookies','$location', function (ng, $http,globalService,$timeout,$cookies,$location) {
		

    if(typeof $cookies.get('AUTHENTICATE')!=='undefined' && typeof $cookies.get('AUTHENTICATE')!==null && $cookies.get('AUTHENTICATE')){

      ng.models = (function(){
        return {
          name : $cookies.get('NAME'),
          steps : [
            {
              visible : true,
              complete : true              
            },
            {
              visible : false,
              complete : false              
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            },
            {
              visible : false,
              complete : false
            }
          ],
          auth : null,
          user : {
            general : false,
            name : null,
            last_name : null,
            second_last_name : null,
            birthdate : null,
            gender : true,
            document_type : null,
            ciu : null,
            nationality : null,
            civil_status :  null,
            contact : {
              email : null,
              cell_phone :  null,
              phone : null
            },
            home : {
              departament : null,
              province:  null,
              district : null,
              urbanization : null,
              via : null,
              address : null
            },
            year_end : null,
            studies : {
              third : {
                school : null,
                district : null
              },
              fourth : {
                school : null,
                district : null
              },
              fifth : {
                school : null,
                district : null
              }
            },
            career : null,
            sede : null,
            representative : {
              name : null,
              last_name : null,
              second_last_name : null,
              birthdate : null,
              gender : true,
              document_type : null,
              ciu : null,
              nationality : null,
              relation : null,
              contact : {
                email : null,
                cell_phone :  null,
                phone : null
              }
            },
            disability : false,
            before_upc : false,
            enrolled : true
          },
          documentTypes : [
            {code:'DNI', name:'DNI'}
          ],
          nationalityList : [],
          civilStatusList : [
            {code:1,name:'Soltero(a)'},
            {code:2,name:'Casado(a)'}
          ],
          departamentList : [],
          provinceList : [],
          districtList : [],
          districtListLima : [],
          viaList : [],
          yearsList : [],
          careerList : [],
          sedeList : [],
          relationList : [
            {code:1,name:'Padre'},
            {code:2,name:'Madre'},
            {code:3,name:'Abuelo'}
          ],
          genderList : [
            {code:1,name:'Masculino'},
            {code:2,name:'Femenino'}
          ],
          schoolList : [],
          isSent : false,
          phrasesList : []
        };
      })();

      ng.models.user.document_type = ng.models.documentTypes[0];        
     
      ng.models.user.representative.gender = ng.models.genderList[0];
      ng.models.user.representative.document_type = ng.models.documentTypes[0];
      
      ng.models.user.representative.relation = ng.models.relationList[0];


      ng.api = (function(){
        return {
          getUser : function(){
            var cf =  {
              headers : {
                'Authorization': 'Token ' + $cookies.get('TOKEN')
              }
            };
            $http.get(config.URL_API + 'applicant/', cf).then(function(response){
              APP.DATA.FN.removeLoadingPage();
              ng.models.auth = response.data;
              var date = ng.models.auth.birth_date;              
              var split = date.split('-');
              var date = new Date(split[0],parseInt(split[1]-1,10),split[2]);
              ng.models.user.ciu = ng.models.auth.dni;
              ng.models.user.general = (ng.models.auth.level===1) ? false : true;
              ng.models.user.name = ng.models.auth.first_name;
              ng.models.user.last_name = ng.models.auth.paternal_last_name;
              ng.models.user.second_last_name = ng.models.auth.maternal_last_name;
              ng.models.user.birthdate = date;
              ng.models.user.gender = (ng.models.auth.gender===1) ? true : false;

              var pos = 0;
              for(var i=0;i<ng.models.nationalityList.length;i++){
                if(ng.models.nationalityList[i].code === ng.models.auth.nationality){
                  pos = i;
                  break;
                }
              }
              ng.models.user.nationality = ng.models.nationalityList[pos];

              ng.models.user.contact.email = ng.models.auth.email;
              ng.models.user.contact.cell_phone = ng.models.auth.phone_mobile;

              pos = 0;
              for(var i=0;i<ng.models.civilStatusList.length;i++){
                if(ng.models.civilStatusList[i].code === ng.models.auth.civil_status){
                  pos = i;
                  break;
                }
              }
              ng.models.user.civil_status = ng.models.civilStatusList[pos];
              ng.models.user.contact.phone = ng.models.auth.phone_home;
              ng.models.user.home.urbanization = ng.models.auth.urbanization;
              ng.models.user.home.address = ng.models.auth.address;


            },function(error){
                APP.DATA.FN.removeLoadingPage();
                if(error.status===401){
                  $cookies.remove('AUTHENTICATE');
                  $cookies.remove('TOKEN');
                  $location.path('login');
                }
            });
          },
          update : function(){

            var date = ng.models.user.birthdate;
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            var dateUser = year + '-' + month + '-' + day;
            date = ng.models.user.representative.birthdate;
            day = date.getDate();
            month = date.getMonth() + 1;
            year = date.getFullYear();
            var dateRepresentative = year + '-' + month + '-' + day;

            var data = {
              dni : ng.models.user.ciu,
              level : (ng.models.user.general) ? 2 : 1,
              first_name : ng.models.user.name,
              paternal_last_name : ng.models.user.last_name,
              maternal_last_name : ng.models.user.second_last_name,
              birth_date : dateUser,
              gender : (ng.models.user.gender) ? 1 : 2,
              nationality : ng.models.user.nationality.code,
              civil_status:ng.models.user.civil_status.code,
              email : ng.models.user.contact.email,
              phone_mobile : ng.models.user.contact.cell_phone,
              phone_home : ng.models.user.contact.phone,
              department : ng.models.user.home.departament.name,
              province : ng.models.user.home.province.name,
              district : ng.models.user.home.district.name,
              urbanization : ng.models.user.home.urbanization,
              way : ng.models.user.home.via.code,
              address : ng.models.user.home.address,
              last_school_year : ng.models.user.year_end.value,
              year_3_school : ng.models.user.studies.third.school.code,
              year_4_school : ng.models.user.studies.fifth.school.code,
              year_5_school : ng.models.user.studies.third.school.code,
              year_3_district : ng.models.user.studies.third.district.name,
              year_4_district : ng.models.user.studies.fourth.district.name,
              year_5_district : ng.models.user.studies.fifth.district.name,
              career : ng.models.user.career.code,
              campus : ng.models.user.sede.code,
              attorney_first_name : ng.models.user.representative.name,
              attorney_paternal_last_name : ng.models.user.representative.last_name,
              attorney_maternal_last_name : ng.models.user.representative.second_last_name,
              attorney_birth_date : dateRepresentative,
              attorney_gender : ng.models.user.representative.gender.code,
              attorney_dni : ng.models.user.representative.ciu,
              attorney_nationality : ng.models.user.representative.nationality.code,
              attorney_relationship : ng.models.user.representative.relation.code,
              attorney_email : ng.models.user.representative.contact.email,
              attorney_phone_home : ng.models.user.representative.contact.phone,
              attorney_phone_mobile : ng.models.user.representative.contact.cell_phone,
              has_disability : ng.models.user.disability,
              has_previous_entry : ng.models.user.before_upc,
              has_enrolled : (ng.models.user.before_upc) ? ng.models.user.enrolled : false
            };
            APP.DATA.FN.showLoadingPage('Registrando...', true);
            var cf =  {
              headers : {
                'Authorization': 'Token ' + $cookies.get('TOKEN')
              }
            };
            $http.patch(config.URL_API + 'applicant/', data, cf).then(function(response){
              APP.DATA.FN.removeLoadingPage();
              if(response.status===200){
                ng.models.isSent = true;
              }            
            },function(error){
              APP.DATA.FN.removeLoadingPage();
                if(error.status===401){
                  $cookies.remove('AUTHENTICATE');
                  $cookies.remove('TOKEN');
                  $location.path('login');
                }else{
                  for(var key in error.data){
                APP.DATA.FN.error(error.data[key][0]);
              }
                }
                            
            });
          },
          getVias : function(){
            $http.get('json/via.json').then(function(response){
              if(response.status===200){
                ng.models.viaList = response.data;
                ng.models.user.home.via = ng.models.viaList[0];
              }
            },function(error){

            });
          },
          getUbigeos : function(){
            $http.get('json/ubigeo-peru.min.json').then(function(response){
              if(response.status===200){
                ng.models.ubigeos = response.data;
                fn.getDepartaments();
                fn.getDistrictsLima();                
              }
            },function(error){

            });
          },
          getCarreras : function(){
            $http.get('json/carreras.json').then(function(response){
              if(response.status===200){
                ng.models.careerList = [];
                angular.forEach(response.data,function(val,key){
                  ng.models.careerList.push({
                    code : val.id,
                    name : val.nombre
                  });
                });
                fn.getCareer();
              }
            },function(error){

            });
          },
          getPhrases : function(){
            $http.get('json/frases-postula.json').then(function(response){
              if(response.status===200){
                ng.models.phrasesList = response.data;
                /*angular.forEach(response.data,function(val,key){
                  ng.models.phrasesList.push({
                    code : val.id,
                    name : val.nombre
                  });
                });*/
                ng.renderSliderFrases = true;
              }
            },function(error){

            });
          },
          getSchools: function () {
          $http.get(config.URL_API + 'commons/schools/').then(function (response) {
            if (response.status === 200) {
              ng.models.schoolList = [];
              angular.forEach(response.data, function (val, key) {
                ng.models.schoolList.push({
                  code: val.cod_empresa,
                  name: val.name
                });
              });
              ng.models.user.studies.third.school = ng.models.schoolList[0];
              ng.models.user.studies.fourth.school = ng.models.schoolList[0];
              ng.models.user.studies.fifth.school = ng.models.schoolList[0];
            }
          }, function (error) {

          });
        },
          getSedes : function(){
            /*$http.get('json/campus.json').then(function(response){
              if(response.status===200){
                ng.models.sedeList = [];
                angular.forEach(response.data,function(val,key){
                  ng.models.sedeList.push({
                    code : val.id,
                    name : val.nombre
                  });
                });
                fn.getSede();
              }
            },function(error){

            });*/
            $http.get(config.URL_API + 'commons/campus/').then(function (response) {
              if (response.status === 200) {
                ng.models.sedeList = [];
                angular.forEach(response.data,function(val,key){
                  ng.models.sedeList.push({
                    code : val.id,
                    name : val.description
                  });
                });
                fn.getSede();
              }
            }, function (error) {

            });
          },
          getNationality: function () {
            $http.get(config.URL_API + 'commons/countries/').then(function (response) {
              if (response.status === 200) {
                ng.models.nationalityList = [];
                angular.forEach(response.data, function (val, key) {
                  ng.models.nationalityList.push({
                    code: val.id,
                    name: val.nationality
                  });
                });
                ng.models.user.nationality = ng.models.nationalityList[0];
                ng.models.user.representative.nationality = ng.models.nationalityList[0];
              }
            }, function (error) {

            });
          }
        };
      })();

      var fn = (function(){
        return {
          getCareer : function(){
            ng.models.user.career = ng.models.careerList[0];
          },
          getSede : function(){
             ng.models.user.sede = ng.models.sedeList[0];
          },
          getDepartaments: function(){
            ng.models.departamentList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.provincia === '00' && val.distrito === '00') {
                ng.models.departamentList.push({
                  code: val.departamento,
                  name : val.nombre
                });
              }
            });
            ng.models.departamentList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.departament = ng.models.departamentList[pos];
            fn.getProvinces(ng.models.user.home.departament.code);
          },
          getProvinces : function(departamento){
            ng.models.provinceList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.departamento === departamento && val.provincia !== '00' && val.distrito === '00') {
                ng.models.provinceList.push({
                  code: val.provincia,
                  name : val.nombre
                });
              }
            });
            ng.models.provinceList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.province = ng.models.provinceList[pos];
            fn.getDistricts(ng.models.user.home.departament.code,ng.models.user.home.province.code);
          },
          getDistricts : function(departamento,provincia){
            ng.models.districtList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.departamento === departamento && val.provincia === provincia && val.distrito !== '00') {
                ng.models.districtList.push({
                  code: val.distrito,
                  name : val.nombre
                });
              }
            });
            ng.models.districtList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.district = ng.models.districtList[pos];
          },
          getDistrictsLima : function(){
            ng.models.districtListLima = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.departamento === '15' && val.provincia === '01' && val.distrito !== '00') {
                ng.models.districtListLima.push({
                  code: val.distrito,
                  name : val.nombre
                });
              }
            });
            ng.models.districtListLima.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.studies.third.district = ng.models.districtListLima[pos];
            ng.models.user.studies.fourth.district = ng.models.districtListLima[pos];
            ng.models.user.studies.fifth.district = ng.models.districtListLima[pos];
          },
          generateYears : function(){
            var year = new Date().getFullYear();
            ng.models.yearsList = [];
            var pos = 0;
            for(var i=0;i<10;i++){
              ng.models.yearsList.push({
                value : year+i
              });
            }
            for(var i=1;i<=10;i++){
              ng.models.yearsList.unshift({
                value : year-i
              });
            }
            ng.models.user.year_end = ng.models.yearsList[pos];
          }
        };
      })();

      ng.onClick = (function(){
        return {
          next : function(pos,isValid){
            if(isValid){
              angular.forEach(ng.models.steps,function(val,key){
                val.visible = false;
                if(key <= pos){
                  val.complete = true;
                }else{
                  val.complete = false;                
                }
              });
              ng.models.steps[pos].visible = true;
              if(pos > 0){
                ng.isDNI = false;
              }else{
                ng.isDNI = true;
              }
            }          
          },
          send : function(isValid){
            if(isValid){
              ng.api.update();
            }
          }
        };
      })();

      ng.onChange = (function(){
        return {
          getProvinces : function(){
            fn.getProvinces(ng.models.user.home.departament.code);
          },
          getDistricts : function(){
            fn.getDistricts(ng.models.user.home.departament.code,ng.models.user.home.province.code);
          }
        };
      })();

      ng.api.getUbigeos();
      ng.api.getCarreras();
      ng.api.getSedes();
      ng.api.getNationality();
      fn.generateYears();
      ng.api.getUser();
      ng.api.getSchools();
      ng.api.getVias();
      ng.api.getPhrases();

      ng.isDNI = true;

    }else{
       $location.path('login');
    }

	}]);

});