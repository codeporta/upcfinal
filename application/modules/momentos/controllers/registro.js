'use strict';

define(['config', 'modules/momentos/init'], function (config, module) {

  var controller = module.name + '.registro';

  module.register.controller(controller, ['$scope', '$http', 'globalService', '$timeout', 'Facebook', '$location','$cookies', function ($scope, $http, globalService, $timeout, Facebook, $location,$cookies) {

    var ng = $scope;



    ng.models = (function () {
      return {
        steps: [
          {
            visible: true
          },
          {
            visible: false
          },
          {
            visible: false
          },
          {
            visible: false
          }
        ],
        user: {
          data: {
            full_name: null,
            last_name: null,
            second_last_name: null,
            birthdate: null,
            nationality: null,
            gender: true,
            ciu: null,
            email: null,
            cell_phone: null,
            interes: null,
            school: null,
            general: 2,
            password : null,
            password_repeat : null
          },
          auth : {},
          home : {
              departament : null,
              province:  null,
              district : null,
              urbanization : null,
              via : null,
              address : null
            }
        },
        careersList: [],
        nationalityList: [],
        sedeList: [],
        schoolList: [],
        levelList : [],
        viaList : [],
        ubigeos : [],
        departamentList : [],
         provinceList : [],
         districtList : [],
         isNormal : false
      };
    })();

    ng.models.user.data.nationality = ng.models.nationalityList[0];

    ng.api = (function () {
      return {
        update : function(){
          if(typeof ng.models.user.auth.token==='undefined' || ng.models.user.auth.token===null){
            APP.DATA.FN.removeLoadingPage();
            APP.DATA.FN.error('Tiene que iniciar el proceso, desde la página principal...');
          }else{
          var date = ng.models.user.data.birthdate;
          var day = date.getDate();
          var month = date.getMonth() + 1;
          var year = date.getFullYear();
          var data = {
            first_name : ng.models.user.data.full_name,
            paternal_last_name : ng.models.user.data.last_name,
            maternal_last_name : ng.models.user.data.second_last_name,
            birth_date : year + '-' + month + '-' + day,
            nationality : ng.models.user.data.nationality.code,
            main_school : ng.models.user.data.school.name,
            phone_mobile : ng.models.user.data.cell_phone,
            email : ng.models.user.data.email,
            gender : (ng.models.user.data.gender) ? 1 : 2,
            level : ng.models.user.data.general,
            dni : ng.models.user.data.ciu,
            m1_form_complete : true,
              department : ng.models.user.home.departament.name,
              province : ng.models.user.home.province.name,
              district : ng.models.user.home.district.name,
              urbanization : ng.models.user.home.urbanization,
              way : ng.models.user.home.via,
              address : ng.models.user.home.address,
              extra_preferred_careers:ng.models.user.data.interes
          };
          APP.DATA.FN.showLoadingPage('Registrando...', true);
          var cf =  {
            headers : {
              'Authorization': 'Token ' + ng.models.user.auth.token
            }
          };
          var d = [];
          angular.forEach(ng.models.careersList, function(val,key){
            if(typeof val.selected!=='undefined' && val.selected){
              d.push(val.id);
            }
          });
          
            $http.put(config.URL_API + 'applicant/preferred-careers/', d, cf).then(function(response){

            },function(error){

            });

            $http.patch(config.URL_API + 'applicant/', data, cf).then(function(response){
            APP.DATA.FN.removeLoadingPage();
            if(response.status===200){
              ng.models.isSent = true;
              $("html, body").stop().animate({ scrollTop: 0 }, '500', 'swing');
                    $cookies.put('AUTHENTICATE', true);
                    $cookies.put('TOKEN', ng.models.user.auth.token);
                    $cookies.put('NAME', data.first_name);
                    $cookies.put('TYPE', data.level);
            }
          },function(error){
            APP.DATA.FN.removeLoadingPage();
          });

        
          }
        },
        register: function (user, password) {
            var data = {
              email : user,
              password :  password
            };
            APP.DATA.FN.showLoadingPage('Registrando usuario...',true);
            $http.post(config.URL_API + 'auth/register-by-email/', data).then(function (response) {
              if(response.status===200){
                //globalService.auth_email = response.data;
                //$location.path('registro');
                ng.models.user.auth.token = response.data.token;
                  
                ng.api.update();
              }
            }, function (error) {
              APP.DATA.FN.removeLoadingPage();
              if(typeof error.data.email!=='undefined'){
                APP.DATA.FN.error(error.data.email[0]);
              }else{
                if(error.data==='email_already_registered'){
                  APP.DATA.FN.error('El correo ya está registrado, inicie sesión...');
                }else{
                  APP.DATA.FN.error('Inténtelo otra vez...');
                }                
              }
            });
          },
        getUbigeos : function(){
            $http.get('json/ubigeo-peru.min.json').then(function(response){
              if(response.status===200){
                ng.models.ubigeos = response.data;
                fn.getDepartaments();            
              }
            },function(error){

            });
          },
        getCareers: function () {
          $http.get('json/carreras.json').then(function (response) {
            if (response.status === 200) {
              ng.models.careersList = response.data;
            }
          }, function (error) {

          });
        },
        getVias : function(){
            $http.get('json/via.json').then(function(response){
              if(response.status===200){
                ng.models.viaList = response.data;
                ng.models.user.home.via = ng.models.viaList[0].code;
              }
            },function(error){

            });
        },
        getSedes: function () {

          $http.get(config.URL_API + 'commons/campus/').then(function (response) {
              if (response.status === 200) {
                ng.models.sedeList = [];
                angular.forEach(response.data,function(val,key){
                  ng.models.sedeList.push({
                    code : val.id,
                    name : val.description,
                    selected: false
                  });
                });
              }
            }, function (error) {

            });
        },
        getSchools: function () {
          $http.get(config.URL_API + 'commons/schools/').then(function (response) {
            if (response.status === 200) {
              ng.models.schoolList = [];
              angular.forEach(response.data, function (val, key) {
                ng.models.schoolList.push({
                  code: val.cod_empresa,
                  name: val.name
                });
              });
              ng.models.user.data.school = ng.models.schoolList[0];
            }
          }, function (error) {

          });
        },
        getLevels: function () {
          $http.get(config.URL_API + 'commons/levels/').then(function (response) {
            if (response.status === 200) {
              ng.models.levelList = response.data;
            }
          }, function (error) {

          });
        },
        getNationality: function () {
          $http.get(config.URL_API + 'commons/countries/').then(function (response) {
            if (response.status === 200) {
              ng.models.nationalityList = [];
              angular.forEach(response.data, function (val, key) {
                ng.models.nationalityList.push({
                  code: val.id,
                  name: val.nationality
                });
              });
              ng.models.user.data.nationality = ng.models.nationalityList[0];
            }
          }, function (error) {

          });
        }
      };
    })();

    var fn = (function(){
      return {
        getDepartaments: function(){
            ng.models.departamentList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.provincia === '00' && val.distrito === '00') {
                ng.models.departamentList.push({
                  code: val.departamento,
                  name : val.nombre
                });
              }
            });
            ng.models.departamentList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.departament = ng.models.departamentList[pos];
            fn.getProvinces(ng.models.user.home.departament.code);
          },
          getProvinces : function(departamento){
            ng.models.provinceList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.departamento === departamento && val.provincia !== '00' && val.distrito === '00') {
                ng.models.provinceList.push({
                  code: val.provincia,
                  name : val.nombre
                });
              }
            });
            ng.models.provinceList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.province = ng.models.provinceList[pos];
            fn.getDistricts(ng.models.user.home.departament.code,ng.models.user.home.province.code);
          },
          getDistricts : function(departamento,provincia){
            ng.models.districtList = [];
            var pos = 0;
            angular.forEach(ng.models.ubigeos,function(val,key){
              if (val.departamento === departamento && val.provincia === provincia && val.distrito !== '00') {
                ng.models.districtList.push({
                  code: val.distrito,
                  name : val.nombre
                });
              }
            });
            ng.models.districtList.unshift({
              code:'00',
              name:'Seleccione'
            });
            ng.models.user.home.district = ng.models.districtList[pos];
          }
      };
    })();

    ng.onChange = (function(){
        return {
          getProvinces : function(){
            fn.getProvinces(ng.models.user.home.departament.code);
          },
          getDistricts : function(){
            fn.getDistricts(ng.models.user.home.departament.code,ng.models.user.home.province.code);
          }
        };
      })();

    ng.onClick = (function () {
      return {
        next: function (pos, isValid) {
          if (isValid) {
            angular.forEach(ng.models.steps, function (val, key) {
              val.visible = false;
              if (key <= pos) {
                val.complete = true;
              } else {
                val.complete = false;
              }
            });
            ng.models.steps[pos].visible = true;
            $("html, body").stop().animate({ scrollTop: 0 }, '500', 'swing');
          }
        },
        send: function (frm) {
          if (frm.$valid) {            
            if(ng.models.isNormal){
              console.log(ng.models.user);
              ng.api.register(ng.models.user.data.email,ng.models.user.data.password);
            }else{
              ng.api.update();
            }
            
          }
        },
        login : function(){
          $location.path('interno');
        }
      }
    })();

    if (typeof globalService.facebook !== 'undefined' && globalService.facebook !== null) {
      console.log('registro por facebook: ',globalService.facebook);
      //ng.api.registerFacebook(globalService.facebook.accessToken);
      //ng.api.getDataFacebook();
      ng.models.user.data.full_name = globalService.facebook.first_name + ' ' + globalService.facebook.middle_name;
      ng.models.user.data.last_name = globalService.facebook.last_name;
      ng.models.user.data.gender = (globalService.facebook.gender === 'male') ? true : false;
      ng.models.user.data.email = globalService.facebook.email;
      ng.models.user.auth = globalService.facebook.auth;
    } else if(typeof globalService.auth_email !== 'undefined' && globalService.auth_email !== null){
      console.log('registro por email', globalService.auth_email);
      ng.models.user.data.email = globalService.auth_email.email;
      ng.models.user.auth = globalService.auth_email;
      APP.DATA.FN.removeLoadingPage();      
    }else{
      console.log('no se ingreso de ningun lado');
      APP.DATA.FN.removeLoadingPage();
      //APP.DATA.FN.error('Tiene que iniciar el proceso, desde la página principal...');
      ng.models.isNormal = true;
    }

    ng.api.getUbigeos();
    ng.api.getCareers();
    ng.api.getSedes();
    ng.api.getSchools();
    ng.api.getLevels();
    ng.api.getNationality();
    ng.api.getVias();

  }]);

});