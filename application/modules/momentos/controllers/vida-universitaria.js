'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.vida-universitaria';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout', function ($scope, $http,globalService,$timeout) {
		
		var ng = $scope;

            ng.models = (function(){
                return {
                    render : null
                };
            })();

            ng.api = (function(){
                return {
                    getVidaUniversitaria : function(){
                        $http.get('json/vida-universitaria.json').then(function(response){
                            if(response.status===200){
                                ng.models.render = response.data;
                            }
                        },function(error){

                        });
                    }
                };
            })();

        ng.api.getVidaUniversitaria();

          $timeout(function(){
            APP.DATA.FN.removeLoadingPage();
          },500);
            
	}]);

});