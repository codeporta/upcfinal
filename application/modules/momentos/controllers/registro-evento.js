'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.registro-evento';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout', function ($scope, $http,globalService,$timeout) {
		
		var ng = $scope;
    var isValid = false;
    if(typeof globalService.evento!=='undefined' && globalService.evento!==null){
        isValid = true;
        ng.models = (function(){
                return {
                   user :{
                    name : null,
                    last_name :  null,
                    second_last_name : null,
                    ciu : null,
                    email : null,
                    phone : null
                   },
                   isSent : false
                };
            })();

            ng.api = (function(){
              return {
                register : function(){
                  var data = {
                    email : ng.models.user.email,
                    first_name : ng.models.user.name,
                    paternal_last_name : ng.models.user.last_name,
                    maternal_last_name : ng.models.user.second_last_name,
                    phone_mobile : ng.models.user.phone,
                    dni : ng.models.user.ciu
                  };
                  APP.DATA.FN.showLoadingPage('Registrando...',true);
                  $http.post(config.URL_API + 'events/'+globalService.evento.id+'/',data).then(function(response){
                    APP.DATA.FN.removeLoadingPage();
                    if(response.status===201){
                      ng.models.isSent = true;
                      $("html, body").stop().animate({ scrollTop: 0 }, '500', 'swing');
                    }
                  },function(error){
                    APP.DATA.FN.removeLoadingPage();
                     APP.DATA.FN.error('Ocurrió un inconveniente, intente otra vez...');
                  });
                }
              };
            })();

            

    }else{
        isValid = false;
        APP.DATA.FN.error('Tiene que seleccionar un evento para registrarse...');
    }


    ng.onClick = (function(){
              return{
                send : function(frm){
                  if(frm.$valid){
                    if(isValid){
                      ng.api.register();  
                    }else{
                      APP.DATA.FN.error('Tiene que seleccionar un evento para registrarse...');
                    }                    
                  }
                }
              }
            })();

    APP.DATA.FN.removeLoadingPage();

	}]);

});