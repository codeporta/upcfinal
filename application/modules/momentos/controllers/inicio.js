'use strict';

define(['config', 'modules/momentos/init'], function (config, module) {

  var controller = module.name + '.inicio';

  module.register.controller(controller, ['$scope', '$http', 'globalService', '$timeout', 'Facebook', '$location', '$cookies', function ($scope, $http, globalService, $timeout, Facebook, $location, $cookies) {

    var ng = $scope;

    if (typeof $cookies.get('AUTHENTICATE') !== 'undefined' && typeof $cookies.get('AUTHENTICATE') !== null && $cookies.get('AUTHENTICATE')) {

      $location.path('interno');

    } else {

    ng.models = (function () {
      return {
        auth : null,
        user : null,
        campus: [],
        facultades: []
      };
    })();

    

    var api = (function () {

      return {
        getFacultades : function(){
          $http.get('json/facultades.json').then(function(response){
            ng.models.facultades = response.data;
            ng.renderFacultadesConvenios = true;
          },function(error){

          });
        },
        getCampus : function(){
          $http.get('json/campus.json').then(function(response){
            ng.models.campus = response.data;
          },function(error){

          });
        },
        registerFacebook: function (token) {
            var data = {
              backend: 'facebook',
              access_token: token
            };
            $http.post(config.URL_API + 'auth/register-by-token/facebook/', data).then(function (response) {
              if (response.status === 200) {             
                api.getUser(response.data);
              }              
            }, function (error) {
              APP.DATA.FN.removeLoadingPage();
              APP.DATA.FN.error('Ocurrió un error');
            });
          },
          getUser : function(data){
            var cf =  {
              headers : {
                'Authorization': 'Token ' + data.token
              }
            };
            $http.get(config.URL_API + 'applicant/',cf).then(function(response){
                APP.DATA.FN.removeLoadingPage();
                if(response.status===200){
                  if(response.data.m1_form_complete){
                    $cookies.put('AUTHENTICATE', true);
                    $cookies.put('TOKEN', data.token);
                    $cookies.put('NAME', response.data.first_name);
                    $location.path('interno');
                  }else{
                    globalService.facebook.auth = data;                 
                    $location.path('registro');
                  }
                }
            },function(error){
              APP.DATA.FN.removeLoadingPage();
              APP.DATA.FN.error('Intentelo otra vez...');
            });
          },
        getDataFacebook: function () {
            Facebook.getLoginStatus(function (response) {
              if (response.status === 'connected') {
                Facebook.api('/me?fields=id,first_name,gender,middle_name,last_name,email,location,locale,permissions', function (response) {
                  if (typeof response.error !== 'undefined') {
                    APP.DATA.FN.error('Inténtelo otra vez...');                    
                    APP.DATA.FN.removeLoadingPage();
                  } else {
                    globalService.facebook = response;                    
                    api.registerFacebook(ng.models.auth.accessToken);
                  }
                });
              }else{
                APP.DATA.FN.removeLoadingPage();
                APP.DATA.FN.error('Error con facebook...');
              }
            });
          }
      };

    })();

   

    ng.onClick = (function () {
      return {
        registerFacebook : function(){
         APP.DATA.FN.showLoadingPage('Conectando con facebook...', true); 
         Facebook.getLoginStatus(function(response) {
          if(response.status === 'connected') {
            ng.models.auth = response.authResponse;
            api.getDataFacebook();
          } else {
            Facebook.login(function(response) {
            if(response.status==='connected'){
              ng.models.auth = response.authResponse;
              api.getDataFacebook();
            }else{
              APP.DATA.FN.removeLoadingPage();
            }
           }, {scope: 'public_profile,email'});
          }
        });
        }
      };
    })();

      
    $timeout(function () {
      APP.DATA.FN.removeLoadingPage();
    }, 2000);

    api.getFacultades();
    api.getCampus();

  }

  }]);

});