'use strict';

define(['modules/momentos/init'], function (module) {

	var controller = module.name + '.404';

	module.register.controller(controller, ['$scope','$http', function ($scope, $http) {
		
		var ng = $scope;

		APP.DATA.FN.removeLoadingPage();
		
	}]);

});