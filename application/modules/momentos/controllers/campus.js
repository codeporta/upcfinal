'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.campus';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout','$routeParams','$location', function ($scope, $http,globalService,$timeout,$routeParams,$location) {
		
		var ng = $scope;            

            ng.models = (function(){
                return {
                    render : null,
                    address : null
                };
            })();

            ng.mapConfig = {};

            ng.api = (function(){
                return {
                    getCampus : function(){
                        $http.get('json/campus/'+$routeParams.uri+'.json').then(function(response){                            
                            if(response.status===200){
                                ng.models.render = response.data;
                                ng.mapConfig = {
                                    zoom : response.data.posicion[0].zoom,
                                    center : response.data.posicion[0].latlong
                                };

                                ng.createMap = true;

                                $timeout(function(){
                                    ng.renderMap = true;
                                    APP.DATA.FN.removeLoadingPage();
                                },500);
                            }
                        },function(error){
                            if(error.status===404){
                                $location.path('404');
                            }
                        });
                    }
                }
            })();            

            ng.api.getCampus();

	}]);

});