'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.testimonios';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout', function ($scope, $http,globalService,$timeout) {
		
		var ng = $scope;

            ng.models = (function(){
                return {
                    render : null
                };
            })();

            ng.api = (function(){
                return {
                    getTestimonios : function(){
                        $http.get('json/testimonios.json').then(function(response){
                            if(response.status===200){
                                ng.models.render = response.data;
                                var videos = [];
                                var images = [];
                                var temp = [];
                                var cont = -1;
                                angular.forEach(response.data.casos.videos,function(val,key){
                                    temp.push(val);
                                    cont++;
                                    if(cont===2){
                                        videos.push(temp);
                                        temp = [];
                                        cont = -1;
                                    }else if(key===response.data.casos.videos.length - 1){
                                        videos.push(temp);
                                    }                                    
                                });
                                temp = [];
                                cont = -1;
                                angular.forEach(response.data.casos.imagenes,function(val,key){
                                    temp.push(val);
                                    cont++;
                                    if(cont===2){
                                        images.push(temp);
                                        temp = [];
                                        cont = -1;
                                    }else if(key===response.data.casos.imagenes.length - 1){
                                        images.push(temp);
                                    }                                    
                                });
                                ng.models.render.casos.videosDesktop = videos;
                                ng.models.render.casos.imagesDesktop = images;
                                ng.renderSliderVideos = true;
                                ng.renderSliderVideosMobile = true;
                                ng.renderSliderImages = true;
                                ng.renderSliderImagesMobile = true;
                            }
                        },function(error){

                        });
                    }
                };
            })();

          ng.api.getTestimonios();

          $timeout(function(){
            APP.DATA.FN.removeLoadingPage();
          },500);
            
	}]);

});