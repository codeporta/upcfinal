'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.facultad';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout', '$routeParams','$location', function ($scope, $http,globalService,$timeout,$routeParams,$location) {
		
		var ng = $scope;

            ng.models = (function(){
                return {
                    render : null
                };
            })();

            ng.api = (function(){
                return {
                    getFacultad : function(){
                        $http.get('json/facultad/'+$routeParams.uri+'.json').then(function(response){
                            if(response.status===200){
                                ng.models.render = response.data;
                                var videos = [];
                                var images = [];
                                var temp = [];
                                var cont = -1;
                                angular.forEach(response.data.casos.videos,function(val,key){
                                    temp.push(val);
                                    cont++;
                                    if(cont===2){
                                        videos.push(temp);
                                        temp = [];
                                        cont = -1;
                                    }else if(key===response.data.casos.videos.length - 1){
                                        videos.push(temp);
                                    }                                    
                                });
                                temp = [];
                                cont = -1;
                                angular.forEach(response.data.casos.imagenes,function(val,key){
                                    temp.push(val);
                                    cont++;
                                    if(cont===2){
                                        images.push(temp);
                                        temp = [];
                                        cont = -1;
                                    }else if(key===response.data.casos.imagenes.length - 1){
                                        images.push(temp);
                                    }                                    
                                });
                                ng.models.render.casos.videosDesktop = videos;
                                ng.models.render.casos.imagesDesktop = images;
                                ng.renderSliderVideos = true;
                                ng.renderSliderVideosMobile = true;
                                ng.renderSliderImages = true;
                                ng.renderSliderImagesMobile = true;
                                APP.DATA.FN.removeLoadingPage();
                            }
                        },function(error){
                            if(error.status===404){
                                $location.path('404');
                            }
                        });
                    }
                };
            })();

            ng.api.getFacultad();

	}]);

});