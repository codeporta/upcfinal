'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.interno';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout','$cookies','$location', function ($scope, $http,globalService,$timeout,$cookies,$location) {
		
		var ng = $scope;

		if(typeof $cookies.get('AUTHENTICATE')!=='undefined' && typeof $cookies.get('AUTHENTICATE')!==null && $cookies.get('AUTHENTICATE')){

					ng.models = (function(){
								return {
									 name : $cookies.get('NAME'),
									 level : parseInt($cookies.get('TYPE'),10),
									 tabs : [
										{active:true},
										{active:false},
										{active:false}
									 ],
									 phrases : [],
									 phrasesRender : false,
									 facultades : [],
									 careerList : [],
									 yearsList : [],
									 schoolList : [],
									 campusList : []
								};
						})();


						ng.api = (function(){
							return {
								getFacultades : function(){
									$http.get('json/facultades.json').then(function(response){
										ng.models.facultades = response.data;
										ng.renderFacultadesConvenios = true;
									},function(error){

									});
								},
								getPhrases : function(){
									$http.get('json/frases.json').then(function(response){
										if(response.status===200){
											ng.models.phrases = response.data;
											ng.models.phrasesRender = true;
										}
									},function(error){

									});
								},
								getCareers: function () {
									$http.get('json/carreras.json').then(function(response){
										if(response.status===200){
											ng.models.careerList = [];
											angular.forEach(response.data,function(val,key){
												ng.models.careerList.push({
													code : val.id,
													name : val.nombre
												});
											});
											ng.models.career = ng.models.careerList[0];
										}
									},function(error){

									});
								},
								getSchools: function () {
					$http.get(config.URL_API + 'commons/schools/').then(function (response) {
						if (response.status === 200) {
							ng.models.schoolList = [];
							angular.forEach(response.data, function (val, key) {
								ng.models.schoolList.push({
									code: val.cod_empresa,
									name: val.name
								});
							});
							ng.models.school = ng.models.schoolList[0];
						}
					}, function (error) {

					});
				},
				
				getCampus : function(){
					$http.get('json/campus.json').then(function(response){
						ng.models.campusList = response.data;
					},function(error){

					});
				}
							};
						})();

						var fn = (function(){
							return {                								              
								generateYears : function(){
									var year = new Date().getFullYear();
									ng.models.yearsList = [];
									var pos = 0;
									for(var i=0;i<10;i++){
										ng.models.yearsList.push({
											value : year+i
										});
									}
									for(var i=1;i<=10;i++){
										ng.models.yearsList.unshift({
											value : year-i
										});
									}
									ng.models.year_end = ng.models.yearsList[pos];
								}
							};
						})();

						ng.onClick = (function(){
							return{
								postule : function(){
									$location.path('postula');
								},
								close : function(){
									$cookies.remove('AUTHENTICATE');
									$cookies.remove('TOKEN');
									$cookies.remove('NAME');
									$location.path('login');
								},
								selectTab : function(pos){
									angular.forEach(ng.models.tabs,function(val,key){
										val.active = false;
									});
									ng.models.tabs[pos].active = true;
								}
							}
						})();


						$timeout(function(){
							APP.DATA.FN.removeLoadingPage();					
						},1000);

						ng.api.getPhrases();
						ng.api.getFacultades();
						ng.api.getCareers();
						fn.generateYears();
						ng.api.getSchools();
						
						ng.api.getCampus();

						ng.renderBecas = true;
						ng.renderFinancial = true;



		}else{
			$location.path('login');
		}

		
	}]);

});