'use strict';

define(['config','modules/momentos/init'], function (config, module) {

	var controller = module.name + '.acreditaciones';

	module.register.controller(controller, ['$scope','$http','globalService','$timeout', function ($scope, $http,globalService,$timeout) {
		
		var ng = $scope;

            ng.models = (function(){
                return {
                    render : null
                };
            })();

            ng.api = (function(){
                return {
                    getAcreditaciones : function(){
                        $http.get('json/acreditaciones.json').then(function(response){
                            if(response.status === 200){
                                ng.models.render = response.data;
                            }
                        },function(error){

                        });
                    }
                };
            })();

            ng.api.getAcreditaciones();

          $timeout(function(){
            APP.DATA.FN.removeLoadingPage();
          },500);
            
	}]);

});