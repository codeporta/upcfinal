'use strict';

define(['config'], function (config) {

    var routeResolver = function () {

        this.$get = function () {
            return this;
        };

        this.routeConfig = function () {
            var viewsDirectory = config.APP + 'modules/' + APP.DATA.CONFIG.MODULE + '/views/',
                controllersDirectory = config.URL_BASE + config.APP + 'modules/' + APP.DATA.CONFIG.MODULE + '/controllers/',

            setBaseDirectories = function (viewsDir, controllersDir) {
                viewsDirectory = viewsDir;
                controllersDirectory = controllersDir;
            },

            getViewsDirectory = function () {
                return viewsDirectory;
            },

            getControllersDirectory = function () {
                return controllersDirectory;
            };

            return {
                setBaseDirectories: setBaseDirectories,
                getControllersDirectory: getControllersDirectory,
                getViewsDirectory: getViewsDirectory
            };
        }();

        this.route = function (routeConfig) {

            var resolve = function (baseName, isNotModule, secure) {
                var routeDef = {};
                routeDef.templateUrl = (isNotModule) ? config.APP + 'views/' + baseName + '.html' : routeConfig.getViewsDirectory() + baseName + '.html';
                routeDef.controller = (isNotModule) ? baseName : config.NAMESPACE + '.' + APP.DATA.CONFIG.MODULE + '.' + baseName;
                routeDef.secure = (secure) ? secure : false;
                routeDef.resolve = {
                    load: ['$q', '$rootScope', function ($q, $rootScope) {
                        var dependencies = [(isNotModule) ? config.URL_BASE + config.APP + 'controllers/'+ baseName + '.js' : routeConfig.getControllersDirectory() + baseName + '.js'];
                        return resolveDependencies($q, $rootScope, dependencies);
                    }]
                };

                return routeDef;
            },

            resolveDependencies = function ($q, $rootScope, dependencies) {
                var defer = $q.defer();
                require(dependencies, function () {
                    defer.resolve();
                    $rootScope.$apply()
                });

                return defer.promise;
            };

            return {
                resolve: resolve
            }
        }(this.routeConfig);

    };

    var servicesApp = angular.module('routeResolverServices', []);

    servicesApp.provider('routeResolver', routeResolver);
});