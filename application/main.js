'use strict';

angular.element(document).ready(function() {

	(function(){
		window.APP = window.APP || {};
		APP.DATA = APP.DATA || {};
		APP.DATA.CONFIG = APP.DATA.CONFIG || {};
		APP.DATA.CONFIG.MODULE = 'momentos';
	})();

	require.config({
		urlArgs : 'v='+(new Date()).getTime(),
		waitSeconds : 0,
		paths : {
			'angular-route' : '../js/angular/angular-route.min',
			'angular-cookies' : '../js/angular/angular-cookies.min'
		}
	});
	require(
		[	
			'config',			
			'data/fn',			
			'services/services',
			'directives/directives',
			'filters/filters',
			'directives/utils',
			'services/services/global',
			'modules/'+ APP.DATA.CONFIG.MODULE +'/init',
			'extra/ngsocialshare',
			'extra/facebook',
			'directives/datepicker',
			'app'
		],
		function(config,a,b,c,d,app){

			angular.bootstrap(angular.element(document), [
				'appModule',				
				config.NAMESPACE + '.services',
				config.NAMESPACE + '.directives',
				config.NAMESPACE + '.filters',
				config.NAMESPACE + '.' + APP.DATA.CONFIG.MODULE,
				'ngsocialshare'				
			]);

			APP.DATA.FN.onReady();

		});

	});
