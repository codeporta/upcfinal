var APP = APP || {};
APP.DATA = APP.DATA || {};
APP.DATA.FN = APP.DATA.FN || {};


//Agregar icono loading
APP.DATA.FN.showLoadingPage = function(msg,inner){
    $('#loading').remove();
    var html = '';
    html +='<div class="loading-mask '+((inner) ? 'loading-mask-inner' : '') +'"></div>';
    html +='<div class="loading">';
        html +='<div class="loading-indicator">';
            html +='<img src="imagenes/loading'+((inner) ? '-inner' : '')+'.gif" width="100" height="100" />';
            html +='<div>';
                html +='<br>';
                html +='<span class="loading-msg" style="color:#fff;">'+msg+'</span>';
            html +='</div>';
        html +='</div>';
    html +='</div>';
    $('body').prepend('<div class="addLoadingPage">'+html+'</div>');
};
//remover icono loading
APP.DATA.FN.removeLoadingPage = function(fn){    
    setTimeout(function(){
        $('body .addLoadingPage').remove();
        if(typeof fn!=='undefined'){
            fn();
        }
    },50);
};

APP.DATA.FN.error = function(msg){
    var html = '';
    html +='<div class="message-error" style="width:100%;height:50px;background:red;color:#fff;position:fixed;top:-100px;left:0;z-index:2;opacity:0.85;cursor:pointer;">';
        html +='<p style="text-align:center;font-size: 14px;line-height: 50px;">'+msg+'</p>';
    html +='</div>';
    $('body').prepend(html);
    $('.message-error').stop().animate({
        'top' : '0px'
    },'fast',function(){
        setTimeout(function(){
            $('.message-error').fadeOut('fast',function(){
                $(this).remove();
            });
        },2000);
    });
};

//funcionalidades al cargar la página por JQuery
APP.DATA.FN.onReady = function(){
    (function(){


            $(window).on('resize', function () {

                console.log('ON RESIZE');

                var height = $(window).height();
                var width = $(window).width();

                if(width >= 960){
                    $('.contenedor-eventos').removeClass('evento-abierto');
                    $('#evento-responsive').addClass('evento-responsive');
                    $('#evento-responsive').removeClass('evento-no-responsive');
                }else{
                    $('#evento-responsive').removeClass('evento-responsive');
                    $('#evento-responsive').addClass('evento-no-responsive');
                    $('.contenedor-eventos').addClass('evento-abierto');
                }
            });

            $(document).on('click','.message-error',function(){
                var t = $(this);
                $(t).fadeOut('fast',function(){
                    $(t).remove();
                });
            });

            $(document).on('click','.eventos .evento-desktop',function(e){
                e.preventDefault();
                $('#close-evento-desktop').fadeIn();
                $('.contenedor-eventos').addClass('evento-abierto');
                $('#evento-responsive').animate({
                    width : '100%'
                },'fast',function(){
                    $(document).trigger('ANIMATE-EVENT-END');
                });
            });

            $(document).on('click','#close-evento-desktop',function(e){
                e.preventDefault();             
                $(this).fadeOut();                
                $('#evento-responsive').animate({
                    width : '0'
                },'fast',function(){
                    $('.contenedor-eventos').removeClass('evento-abierto');
                    $(document).trigger('ANIMATE-EVENT-RESET');
                });
            });
            
            $(document).on('mouseenter','.caso-exito-video',function(e){
                $(this).stop().animate({
                    'opacity' : 1                   
                },'fast');
            });
            $(document).on('mouseleave','.caso-exito-video',function(e){
                $(this).stop().animate({
                    'opacity' : 0.50                    
                },'fast');
            });



            $(document).on('click','#link-admision-preferente',function(e){
                e.preventDefault();
                $(this).hide();
                $('#view-admision-preferente-desktop').fadeIn();
            });

            $(document).on('mouseleave','#view-admision-preferente-desktop',function(e){
                e.preventDefault();
                $(this).hide();
                $('#link-admision-preferente').fadeIn();
            });

            $(document).on('click','#link-admision-general',function(e){
                e.preventDefault();
                $(this).hide();
                $('#view-admision-general-desktop').fadeIn();
            });
            $(document).on('mouseleave','#view-admision-general-desktop',function(e){
                e.preventDefault();
                $(this).hide();
                $('#link-admision-general').fadeIn();
            });

            $(document).on('click','#btn-convenios-slider-desktop',function(e,d){
                e.preventDefault();
                var width = $(window).width();
                var that = $(this);
                
                if(typeof d!=='undefined' && d.isClose){
                    if(that.hasClass('open')){
                        that.removeClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                        if(width >= 960){
                            $('#view-slider-convenios-container-desktop').hide();                        
                        }else if(width >= 768){

                        }else{

                        }
                    }
                }else{
                    $('#btn-campus-slider-desktop').trigger('click',{isClose:true});
                    $('#btn-facultades-slider').trigger('click',{isClose:true});

                    if(that.hasClass('open')){
                        that.removeClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                        if(width >= 960){
                            $('#view-slider-convenios-container-desktop').slideUp('fast');                        
                        }else if(width >= 768){

                        }else{

                        }
                    }else{
                        that.addClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                        if(width >= 960){
                            $('#view-slider-convenios-container-desktop').slideDown('fast',function(){
                                $('#mapaConvenios').vectorMap('get','mapObject').updateSize();
                            });
                            
                        }else if(width >= 768){

                        }else{

                        }
                    }
                }

                
            });

            $(document).on('click','#btn-campus-slider-desktop',function(e,d){
                e.preventDefault();
                var width = $(window).width();
                var that = $(this);

                if(typeof d!=='undefined' && d.isClose){
                    if(that.hasClass('open')){
                        that.removeClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                        if(width >= 960){
                            $('#view-slider-campus-container-desktop').hide();
                        }else if(width >= 768){

                        }else{
                            
                        }
                    }
                }else{

                    $('#btn-convenios-slider-desktop').trigger('click',{isClose:true});
                    $('#btn-facultades-slider').trigger('click',{isClose:true});

                     if(that.hasClass('open')){
                        that.removeClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                        if(width >= 960){
                            $('#view-slider-campus-container-desktop').slideUp('fast');
                        }else if(width >= 768){

                        }else{
                            
                        }
                    }else{
                        that.addClass('open');
                        $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                        if(width >= 960){
                            $('#view-slider-campus-container-desktop').slideDown('fast');
                        }else if(width >= 768){

                        }else{
                            
                        }
                    }   
                }

            });


                $(document).on('click','#btn-facultades-slider',function(e,d){
                    e.preventDefault();
                    var width = $(window).width();
                    var that = $(this);

                     if(typeof d!=='undefined' && d.isClose){
                        if(that.hasClass('open')){
                            that.removeClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                            if(width >= 1000){
                                $('#view-slider-facultades-container-desktop').hide();
                            }else{
                                $('#view-slider-facultades-container-mobile').slideUp('fast');
                            }
                        }
                     }else{

                        $('#btn-campus-slider-desktop').trigger('click',{isClose:true});
                        $('#btn-convenios-slider-desktop').trigger('click',{isClose:true});

                        if(that.hasClass('open')){
                            that.removeClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                            if(width >= 1000){
                                $('#view-slider-facultades-container-desktop').slideUp('fast');
                            }else{
                                $('#view-slider-facultades-container-mobile').slideUp('fast');
                            }
                        }else{
                            that.addClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                            if(width >= 1000){
                                $('#view-slider-facultades-container-desktop').slideDown('fast');
                            }else{
                                $('#view-slider-facultades-container-mobile').slideDown('fast');
                            }
                        }
                     }
                    

                });

        
                $(document).on('click','#btn-convenios-slider-mobile',function(e,d){
                    e.preventDefault();
                    var that = $(this);
                    if(that.hasClass('open')){
                            that.removeClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                            $('#view-slider-convenios-container-mobile').slideUp('fast');
                        }else{
                            that.addClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                            $('#view-slider-convenios-container-mobile').slideDown('fast');
                        }
                    
                });

                $(document).on('click','#btn-campus-slider-mobile',function(e,d){
                    e.preventDefault();
                    var that = $(this);
                    if(that.hasClass('open')){
                            that.removeClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                            $('#view-slider-campus-container-mobile').slideUp('fast');
                        }else{
                            that.addClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                            $('#view-slider-campus-container-mobile').slideDown('fast');
                        }
                    
                });


                $(document).on('click','#btn-campus-slider-tablet',function(e,d){
                    e.preventDefault();
                    var that = $(this);
                    if(that.hasClass('open')){
                            that.removeClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-down-2.png');
                            $('#view-slider-campus-container-tablet').slideUp('fast');
                        }else{
                            that.addClass('open');
                            $(that).find('.button-small').attr('src','imagenes/button-up-2.png');
                            $('#view-slider-campus-container-tablet').slideDown('fast');
                        }
                    
                });

                $(document).on('click','#btn_profile',function(e){
                    e.preventDefault();
                    if($(this).hasClass('view_profile')){
                        $(this).removeClass('view_profile');
                        $(this).find('i').removeClass('fa-chevron-up');
                        $(this).find('i').addClass('fa-chevron-down');
                        $('#btn_profile_view').slideUp();
                    }else{
                        $(this).addClass('view_profile');
                        $('#btn_profile_view').slideDown();
                       
                        $('#btn_financial_view').slideUp();
                        $('#btn_financial').removeClass('view_financial');
                        $('#btn_financial').find('i').removeClass('fa-chevron-up');
                        $('#btn_financial').find('i').addClass('fa-chevron-down');
                        
                        $(this).find('i').removeClass('fa-chevron-down');
                        $(this).find('i').addClass('fa-chevron-up');
                    }                    
                });

                $(document).on('click','#btn_financial',function(e){
                    e.preventDefault();
                    if($(this).hasClass('view_financial')){
                        $(this).removeClass('view_financial');
                        $(this).find('i').removeClass('fa-chevron-up');
                        $(this).find('i').addClass('fa-chevron-down');
                        $('#btn_financial_view').slideUp();
                    }else{
                        $(this).addClass('view_financial');

                        $('#btn_profile_view').slideUp();
                        $('#btn_profile').removeClass('view_profile');
                        $('#btn_profile').find('i').removeClass('fa-chevron-up');
                        $('#btn_profile').find('i').addClass('fa-chevron-down');
                        
                        $('#btn_financial_view').slideDown();
                        $(this).find('i').removeClass('fa-chevron-down');
                        $(this).find('i').addClass('fa-chevron-up');
                    } 
                });


    })();
};