'use strict';

define(['config','modules/momento2/init'], function (config,module) {

	var controller = module.name + '.momento2';

	module.register.controller(controller, ['$scope','$http', 'ngDialog','$sce','globalService','$timeout', function ($scope, $http, ngDialog,$sce,globalService,$timeout) {
		
		var ng = $scope;

		ng.models = (function(){
			return {
				period : {
					remaining_days : null,
					description : null
				},
				pension : {
					category_code : null,
					cycle_cost : null
				},
				viewSimulator : false,
				career :  null
			};
		})();

		ng.onClick = (function(){
			return{
				modal : function(id){
					if(id==='documentation'){
						if(globalService.applicant!==null){
							if(globalService.applicant.level === config.CONSTANTS.STUDENT.PREFERENTE.id){
								id = id + '-' + config.CONSTANTS.STUDENT.PREFERENTE.description;
							}else if(globalService.applicant.level === config.CONSTANTS.STUDENT.REGULAR.id){
								id = id + '-' + config.CONSTANTS.STUDENT.REGULAR.description;
							}
						}
					}
					ng.pdf = $sce.trustAsResourceUrl('public/pdf/'+id+'.pdf');
					ng.showLoadingIframe = true;
					ngDialog.open({ template: 'templateIframeId' , closeByDocument :false, width: '80%', height: 600, scope: ng});
					$timeout(function(){
						ng.showLoadingIframe = false;
					},750);
				},
				viewSimulator : function(){
					api.getPension();
				}
			};
		})();

		var api = (function(){
			return {
				getPension : function(){
					ng.showLoadingPension = true;
					$http.get('pension/').then(function(response){
						ng.models.pension.category_code = response.data.category_code;
						ng.models.pension.cycle_cost = response.data.cycle_cost;
						ng.showLoadingPension = false;
						ng.models.viewSimulator = true;
					},function(error){

					});
				},
				getPeriod : function(){
					$http.get('period/').then(function(response){
						ng.models.period.remaining_days = response.data.remaining_days;
						ng.models.period.description = response.data.description;
					},function(error){

					});
				},
				getCareer : function(){
					$http.get('career/').then(function(response){
						ng.models.career = response.data;
						var tempTotal = [];
						var temp = [];
						var cont = 0;
						angular.forEach(ng.models.career.graduated_profiles,function(val,key){					
							val.styleProfile = {'background': 'url('+val.image+') 0 0 no-repeat'};
							if(cont < 2){
								temp.push(val);
								cont++;
							}
							if(cont===2){
								tempTotal.push(angular.copy(temp));
								temp = [];
								cont = 0;
							}
						});
						ng.models.career.graduated_profiles = tempTotal;
					},function(error){

					});
				}
			};
		})();

		api.getCareer();
		api.getPeriod();
		
	}]);

});