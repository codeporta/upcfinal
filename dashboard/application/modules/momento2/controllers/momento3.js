'use strict';

define(['config','modules/momento2/init'], function (config,module) {

	var controller = module.name + '.momento3';

	module.register.controller(controller, ['$scope','$http', 'ngDialog','$sce','Socialshare','globalService','$timeout', function ($scope, $http, ngDialog,$sce,Socialshare,globalService,$timeout) {
		
		var ng = $scope;

		ng.models = (function(){
			return {
				period : {
					remaining_days : null,
					description : null
				},
				pension : {
					category_code : null,
					cycle_cost : null
				},
				viewSimulator : false,
				eventsAvailable : {
					data : [],
					loading : {
						isResults : false,
						config :  {
							text : 'Cargando eventos...',
							view : false
						}
					}
				},
				eventsRegistered : {
					data : [],
					loading : {
						isResults : false,
						config :  {
							text : 'Cargando agenda...',
							view : false
						}
					}
				}
			};
		})();

		var fn = (function(){
			return {
				getHours : function(date){
					var d = new Date(date);
					var hour = d.getHours();
					var minutes = d.getMinutes();
					var am = 'AM';
					if(hour < 10){
						hour = '0'+hour;
					}
					if(minutes < 10){
						minutes = '0'+minutes;
					}
					return hour + ':' + minutes + ' h.';
				}
			};
		})();

		ng.onClick = (function(){
			return{
				modal : function(id){
					if(id==='documentation'){
						if(globalService.applicant!==null){
							if(globalService.applicant.level === config.CONSTANTS.STUDENT.PREFERENTE.id){
								id = id + '-' + config.CONSTANTS.STUDENT.PREFERENTE.description;
							}else if(globalService.applicant.level === config.CONSTANTS.STUDENT.REGULAR.id){
								id = id + '-' + config.CONSTANTS.STUDENT.REGULAR.description;
							}
						}
					}
					ng.pdf = $sce.trustAsResourceUrl('public/pdf/'+id+'.pdf');
					ng.showLoadingIframe = true;
					ngDialog.open({ template: 'templateIframeId' , closeByDocument :false, width: '80%', height: 600, scope: ng});
					$timeout(function(){
						ng.showLoadingIframe = false;
					},750);
				},
				viewSimulator : function(){
					api.getPension();
				}
			};
		})();

		var api = (function(){
			return {
				getPeriod : function(){
					$http.get('period/').then(function(response){
						ng.models.period.remaining_days = response.data.remaining_days;
						ng.models.period.description = response.data.description;
					},function(error){

					});
				},
				getPension : function(){
					ng.showLoadingPension = true;
					$http.get('pension/').then(function(response){
						ng.models.pension.category_code = response.data.category_code;
						ng.models.pension.cycle_cost = response.data.cycle_cost;
						ng.showLoadingPension = false;
						ng.models.viewSimulator = true;
					},function(error){

					});
				},
				getEventsAvailable : function(){
					ng.models.eventsAvailable.loading.config.view = true;
					$http.get('events/available/').then(function(response){
						if(response.status===200){
							ng.models.eventsAvailable.loading.config.view = false;
							ng.models.eventsAvailable.data = response.data;
							angular.forEach(ng.models.eventsAvailable.data,function(val,key){
								angular.forEach(val.events,function(v,k){
									switch(v.event_type.id){
										case 1:
											v.styleEvent = {'background': '#fabe15 url(statics/img/img_event.jpg) 0 100% no-repeat'};
										break;
										case 2:
											v.styleEvent = {'background': '#808080 url(statics/img/img_event.jpg) 0 100% no-repeat'};
										break;
										case 3:
											v.styleEvent = {'background': '#0072b5 url(statics/img/img_event.jpg) 0 100% no-repeat'};
										break;
									}
									v.starts_at_hour = fn.getHours(v.starts_at);
									v.ends_at_hour = fn.getHours(v.ends_at);
								});
							});							
							if(ng.models.eventsAvailable.data.length > 0){
								ng.models.eventsAvailable.loading.isResults = true;
							}else{
								ng.models.eventsAvailable.loading.isResults = false;
							}
						}
					
					},function(error){

					});
				},
				getEventsRegistered : function(){
					ng.models.eventsRegistered.loading.config.view = true;
					$http.get('events/registered/').then(function(response){
						if(response.status===200){
							ng.models.eventsRegistered.loading.config.view = false;
							ng.models.eventsRegistered.data = response.data;
							angular.forEach(ng.models.eventsRegistered.data,function(val,key){
								angular.forEach(val.applicant_events,function(v,k){
									switch(v.event.id){
										case 1:
											val.styleEvent = {'background-color': '#fabe15'};
										break;
										case 2:
											val.styleEvent = {'background-color': '#808080'};
										break;
										case 3:
											val.styleEvent = {'background-color': '#0072b5'};
										break;
									}
									v.event.starts_at_hour = fn.getHours(v.event.starts_at);
									v.event.ends_at_hour = fn.getHours(v.event.ends_at);
								});
							});

							if(ng.models.eventsRegistered.data.length > 0){
								ng.models.eventsRegistered.loading.isResults = true;
							}else{
								ng.models.eventsRegistered.loading.isResults = false;
							}
						}
					
					},function(error){
						ng.models.eventsRegistered.loading.isResults = false;
					});
				},
				saveEventsRegistered : function(id){
					var data = {
						event_id : id
					};
					$http.post('events/registered/new/', data).then(function(response){
						if(response.status===201){
							api.getEventsRegistered();
							api.getEventsAvailable();
						}
					},function(error){
						
					});
				},
				deleteEventsRegistered : function(id){
					$http.delete('events/registered/'+id+'/').then(function(response){
						if(response.status===204){
							api.getEventsRegistered();
							api.getEventsAvailable();
						}
					},function(error){
						
					});
				}
			};
		})();

		ng.onEvents = (function(){
			return {
				onCloseDiary : function(id){
					api.deleteEventsRegistered(id);
				},
				onCloseEvent : function(id){
					api.saveEventsRegistered(id);
				}
			};
		})();

		api.getPeriod();
		api.getEventsAvailable();
		api.getEventsRegistered();
		
	}]);

});