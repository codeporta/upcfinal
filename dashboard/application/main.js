'use strict';

angular.element(document).ready(function() {

	(function(){
		window.APP = window.APP || {};
		APP.DATA = APP.DATA || {};
		APP.DATA.CONFIG = APP.DATA.CONFIG || {};
		APP.DATA.CONFIG.MODULE = 'momento2';
	})();

	require.config({
		urlArgs : 'v='+(new Date()).getTime(),
		waitSeconds : 0,
		paths : {
			'angular-route' : '../statics/js/angular/angular-route.min',
			'angular-animate' : '../statics/js/angular/angular-animate.min',
			'angular-resource' : '../statics/js/angular/angular-resource.min',
			'angular-cookies' : '../statics/js/angular/angular-cookies.min'
		}
	});
	require(
		[	
			'config',			
			'data/fn',			
			'services/services',
			'directives/directives',
			'filters/filters',
			'directives/utils',
			'services/services/global',
			'modules/'+ APP.DATA.CONFIG.MODULE +'/init',
			'extra/ngDialog',
			'extra/ngsocialshare',
			'app'
		],
		function(config,a,b,c,d,app){

			angular.bootstrap(angular.element(document), [
				'appModule',				
				config.NAMESPACE + '.services',
				config.NAMESPACE + '.directives',
				config.NAMESPACE + '.filters',
				config.NAMESPACE + '.' + APP.DATA.CONFIG.MODULE,
				'ngDialog',
				'ngsocialshare'
			]);

			APP.DATA.FN.showMessageStart('CODE001');
			APP.DATA.FN.removeMessageStart('dashboard');
			APP.DATA.FN.onReady();

		});

	});
