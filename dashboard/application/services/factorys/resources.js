'use strict';

define(['services/services'], function (module) {

  	module.factory('modulesRest',function($resource){
  		return $resource('module/:id', {id:'@csm'});
  	});

});
