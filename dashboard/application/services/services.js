'use strict';

define(['config'], function (config) {
	var module = angular.module(config.NAMESPACE + '.services',[]);
	return module;
});