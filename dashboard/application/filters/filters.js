'use strict';

define(['config'], function (config) {
	var module = angular.module(config.NAMESPACE + '.filters',[]);

  module.filter('dateLocale', [function () {
    return function (str) {
      return moment(str).locale('es').format('LL');
    }
  }]);

  module.filter('applicantLevelLabel', [function () {
    return function (id) {
      if (id == config.CONSTANTS.STUDENT.PREFERENTE.id) {
        return config.CONSTANTS.STUDENT.PREFERENTE.description;
      }

      if (id == config.CONSTANTS.STUDENT.REGULAR.id) {
        return config.CONSTANTS.STUDENT.REGULAR.description;
      }

      return '';
    }
  }]);

  return module;

});