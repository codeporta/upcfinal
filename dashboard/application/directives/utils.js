'use strict';

define(['config','directives/directives'], function(configGlobal, module) {


    module.constant('validatorsConfig', {
        RUC: { reg: /^[0-9]{11}$/, val: 'uiruc' },
        EMAIL: { reg: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/, val: 'uiemail' },
        DNI: { reg: /^[0-9]{8}$/, val: 'uidni' },
        PASAPORTE: { reg: /^[0-9]{15}$/, val: 'uipasaporte' },
        CARNET: { reg: /^[0-9]{20}$/, val: 'uicarnet' },
        INTEGER: { reg: /^\-?\d*$/, val: 'uiinteger' },
        FLOAT: { reg: /^\-?\d+((\.|\,)\d+)?$/, val: 'uifloat' },
        UBIGEO: { reg: /^[0-9]{6}$/, val: 'uiubigeo' },
        PLAN: { reg: /^([0-9])+(\.)([0-9]){2}$/, val: 'uiplan' },
        DECIMAL: { reg: /^\-?\d+((\.)\d+)?$/, val: 'uidecimal' },
    });

    module.directive('uiValid', ['validatorsConfig', function(config) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {

                ctrl.$parsers.unshift(function(viewValue) {

                    if (config[attrs.uiValid].reg.test(viewValue)) {
                        ctrl.$setValidity(config[attrs.uiValid].val, true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity(config[attrs.uiValid].val, false);
                        return undefined;
                    }
                });

                ctrl.$formatters.unshift(function(viewValue) {

                    if (config[attrs.uiValid].reg.test(viewValue)) {
                        ctrl.$setValidity(config[attrs.uiValid].val, true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity(config[attrs.uiValid].val, false);
                        return viewValue;
                    }
                });

            }
        };
    }]);



    module.directive('error', function() {

        return {
            restrict: 'EA',
            replace: true,
            scope: {
                uiError: '='
            },
            template: '' +
            '<div data-ng-show="uiError" class="alert alert-danger m10">' +
            '<i class="fa fa-remove pr10"></i>' +
            ' Ocurrió un error en el servidor.' +
            '</div>'

        };

    });

    module.directive('uiCloseDiary', function() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                uiId : '=',
                onClose:'&'
            },
            link: function(scope, element, attrs, ctrl) {
                $(element).click(function(e){
                    e.preventDefault();
                    $(this).parent().parent().slideUp('fast',function(){
                        scope.onClose({id:scope.uiId});
                    });
                });
            }
        };
    });
    module.directive('uiCloseEvent', function() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                uiId : '=',
                onClose:'&'
            },
            link: function(scope, element, attrs, ctrl) {
                $(element).click(function(e){
                    e.preventDefault();
                    $(this).parent().fadeOut('fast',function(){
                        scope.onClose({id:scope.uiId});
                    });
                });
            }
        };
    });

    module.directive('loading', function() {

        return {
            restrict: 'EA',
            replace: true,
            scope: {
                uiConfig: '='
            },
            link: function(scope, element, attrs, ctrl) {
                scope.$watchCollection('uiConfig', function(newValue, oldValue) {
                    if (typeof newValue !== 'undefined') {
                        scope.sty = (scope.uiConfig.isTable) ? { 'text-align': 'center', 'padding': '30px 0', 'position': 'absolute', 'left': '50%', 'top': '50%', 'margin-top': '-50px', 'margin-left': '-100px', 'z-index': '10' } : { 'text-align': 'center', 'padding': '30px 0' };
                    }
                });

            },
            template: '<div data-ng-show="uiConfig.view">' +
            '<div data-ng-show="uiConfig.isTable" style="background-color: #ffffff;width: 100%;height: 100%;position: absolute;opacity: 0.9;z-index: 10;"></div>' +
            '<div data-ng-show="!uiConfig.isTable" data-ng-style="sty">' +
            '<img src="'+configGlobal.URL_BASE+'statics/img/loading.gif" width="25" height="25"><br>' +
            '<span style="font-size:15px;color:#3498db">{{uiConfig.text}}</span>' +
            '</div></div>'
        };

    });

   

    module.directive('debug', function($filter) {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                modelo: '='
            },
            link: function(scope, element, attrs) {

                scope.nombreModelo = attrs.modelo;
                scope.leftPos = 0;

                $(element).find('button').on('click', function(e) {
                    e.stopPropagation();
                    $(element).find('button').addClass('encima');
                    scope.mostrar = true;
                    scope.leftPos = $(element).find('button').width() + 50;
                    $(element).children('div').addClass('codeShow');
                    scope.$apply();
                });

                $(element).find('button').on('mouseover', function(e) {
                    if (!$(element).children('div').hasClass('codeShow')) {
                        $(element).find('button').addClass('encima');
                        scope.mostrar = true;
                        scope.leftPos = $(element).find('button').width() + 50;
                        scope.$apply();
                    }
                });
                $(element).find('button').on('mouseout', function(e) {
                    if (!$(element).children('div').hasClass('codeShow')) {
                        $(element).find('button').removeClass('encima');
                        scope.mostrar = false;
                        scope.$apply();
                    }
                });

                $(document).on('click', function(e) {
                    $(element).find('button').removeClass('encima');
                    scope.mostrar = false;
                    $(element).children('div').removeClass('codeShow');
                    scope.$apply();
                });

                $(element).find('pre').on('click', function(e) {
                    e.stopPropagation();
                });

            },
            template: '<div style="position:relative;display:inline-block;">' +
            '<style>pre code{display:block;padding:.5em;background:#002451;font-size:13px;color:#adff2f!important}pre .tag,pre code{color:#f8f8f2}pre .change,pre .clojure .built_in,pre .flow,pre .function,pre .keyword,pre .lisp .title,pre .literal,pre .nginx .title,pre .tex .special,pre .winutils{color:#66d9ef}pre .params,pre .variable{color:#fd9720}pre .constant{color:#66d9ef}pre .class .title,pre .css .class,pre .title{color:#a6e22e}pre .attribute,pre .css .tag,pre .symbol,pre .symbol .string,pre .tag .title,pre .value{color:#f92672}pre .number,pre .preprocessor,pre .regexp{color:#ae81ff}pre .addition,pre .apache .cbracket,pre .apache .tag,pre .attr_selector,pre .built_in,pre .css .id,pre .django .filter .argument,pre .django .template_tag,pre .django .variable,pre .envvar,pre .haskell .type,pre .prompt,pre .pseudo,pre .ruby .class .parent,pre .smalltalk .array,pre .smalltalk .class,pre .smalltalk .localvars,pre .sql .aggregate,pre .stream,pre .string,pre .subst,pre .tag .value,pre .tex .command{color:#e6db74}pre .apache .sqbracket,pre .comment,pre .deletion,pre .doctype,pre .java .annotation,pre .javadoc,pre .pi,pre .python .decorator,pre .shebang,pre .template_comment,pre .tex .formula{color:#75715e}pre .coffeescript .javascript,pre .javascript .xml,pre .tex .formula,pre .xml .cdata,pre .xml .css,pre .xml .javascript,pre .xml .vbscript{opacity:.5}.encima{background:#002451 !important;color:greenyellow !important;}</style>' +
            '<button class="btn btn-inverse" type="button"><i class="icon-thumbs-up"></i> {{nombreModelo}}</button>' +
            '<div class="well" style="text-align:left;width:800px;position:absolute;z-index:9999;top:0px;left:{{leftPos}}px;max-height:500px;overflow-y: auto;padding: 5px;" data-ng-show="mostrar">' +
            '<pre style="background:transparent;border:none;"><code class="json">{{modelo | json}}</code></pre>' +
            '</div>' +
            '</div>'
        };
    });




    module.directive('fileUpload', function(globalFactory, $http, toast) {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                config: '=',
                render: '=',
                update: '='
            },
            link: function(scope, element, attrs) {

                var ng = scope;

                ng.URL_BASE = APP.DATA.CONFIG.URL_BASE;

                if (typeof ng.config === 'undefined') {
                    ng.disabled = true;
                    $(element).find('.showMessage').stop().hide();
                    console.error('data-config is not defined');
                    return false;
                } else if (typeof ng.config.maxFiles === 'undefined') {
                    ng.disabled = true;
                    $(element).find('.showMessage').stop().hide();
                    console.error('maxFiles is not defined');
                    return false;
                } else if (typeof ng.config.maxSize === 'undefined') {
                    ng.disabled = true;
                    $(element).find('.showMessage').stop().hide();
                    console.error('maxSize is not defined');
                    return false;
                } else if (typeof ng.config.fileTypes === 'undefined') {
                    ng.disabled = true;
                    $(element).find('.showMessage').stop().hide();
                    console.error('fileTypes is not defined');
                    return false;
                } else if (typeof ng.config.alias === 'undefined') {
                    ng.disabled = true;
                    $(element).find('.showMessage').stop().hide();
                    console.error('alias is not defined');
                    return false;
                }

                ng.view = {
                    loading: {
                        text: 'Cargando archivos...',
                        isTable: false
                    },
                    show: {
                        loading: true
                    }
                };


                ng.queue = [];

                var fn = {
                    validateExtension: function(name) {
                        var txt = '';
                        angular.forEach(ng.config.fileTypes, function(val, key) {
                            txt += val + '|';
                        });
                        txt = txt.substring(0, txt.length - 1);
                        var regex = new RegExp("(\.)(" + txt + ")");
                        return regex.test(name);
                    },
                    toBytes: function(bytes) {
                        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                        if (bytes == 0) return '0 Byte';
                        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
                    },
                    renderShowMessage: function(show) {
                        if (show) {
                            $(element).find('.showMessage').stop().fadeIn('fast', function() {
                                var self = $(this);
                                setTimeout(function() {
                                    self.stop().fadeOut('fast');
                                }, 2000);
                            });
                        } else {
                            $(element).find('.showMessage').stop().hide();
                        }
                    },
                    getElementArray: function(hashKey) {
                        var pos = -1;
                        for (var i = 0; i < ng.queue.length; i++) {
                            if (ng.queue[i].hashKey === hashKey) {
                                pos = i;
                                break;
                            }
                        }
                        return pos;
                    },
                    createHash: function(e) { for (var r = 0, i = 0; i < e.length; i++)r = (r << 5) - r + e.charCodeAt(i), r &= r; return r },
                    render: function() {
                        if (typeof ng.config.files !== 'undefined') {
                            ng.queue = [];
                            angular.forEach(ng.config.files, function(val, key) {
                                var hash = 'ID_' + new Date().getTime() + '_' + fn.createHash(val.name);
                                val.isFileLoaded = true;
                                var myFile = {};
                                myFile.file = val;
                                myFile.status = {
                                    isCancel: false,
                                    isError: false,
                                    isDone: true
                                };
                                myFile.hashKey = hash;
                                myFile.msg = 'Archivo ya almacenado.';
                                ng.queue.push(myFile);
                            });
                            ng.disabled = false;
                            ng.view.show.loading = false;
                        }
                    },
                    update: function() { // update directive values
                        var txt = '';
                        angular.forEach(ng.config.fileTypes, function(val, key) {
                            txt += '.' + val + ', ';
                        });
                        txt = txt.substring(0, txt.length - 2);
                        ng.description = 'Se acepta ' + ng.config.maxFiles + ' archivo(s), con extensión (' + txt + ') y un tamaño máximo de ' + fn.toBytes(ng.config.maxSize);
                    },
                    removeQueue: function(hash) {
                        var pos = fn.getElementArray(hash);
                        if (pos !== -1) {
                            ng.queue.splice(pos, 1);
                        }
                    },
                    removeFile: function(item) {
                        item.isLoadingRemove = true;
                        var itemCopy = angular.copy(item);
                        var data = {};
                        angular.extend(data, ng.config.formData);
                        data.newName = itemCopy.file.newName;
                        $http.post(ng.config.alias + '/remove', data).
                            success(function(data, status, headers, config) {
                                globalFactory.close();
                                if (status === 200) {
                                    item.isLoadingRemove = false;
                                    fn.removeQueue(itemCopy.hashKey);
                                    ng.$emit('FILE_UPLOAD_REMOVE', { file: angular.copy(itemCopy.file), queue: angular.copy(ng.queue.length), key : ng.config.key });
                                }
                            }).
                            error(function(data, status, headers, config) {
                                globalFactory.close();
                                item.isLoadingRemove = false;
                                switch (status) {
                                    case 404:
                                        toast.show('El archivo no existe', 'error');
                                        break;
                                    case 500:
                                        toast.show('No se pudo eliminar el archivo', 'error');
                                        break;
                                }
                            });
                    }
                };

                fn.renderShowMessage(false);

                fn.update();

                ng.onClick = {
                    close: function(item) {
                        if (item.status.isCancel && !item.status.isError && !item.status.isDone) {
                            item.jqXHR.abort();
                            fn.removeQueue(item.hashKey);
                        } else if (!item.status.isCancel && !item.status.isError && item.status.isDone) {
                            globalFactory.showConfirm(function() {
                                fn.removeFile(item);
                                ng.$apply();
                            }, 'Seguro que desea eliminar este archivo?');
                        } else {
                            fn.removeQueue(item.hashKey);
                        }
                    }
                };
                $(element).fileupload({
                    url: APP.DATA.CONFIG.URL_BASE + ng.config.alias + '/upload',
                    autoUpload: false,
                    dataType: 'json',
                    add: function(e, data) {
                        fn.renderShowMessage(false);
                        if (typeof data.files !== 'undefined' && data.files.length > 0) {
                            var file = data.files[0];
                            if (ng.queue.length >= ng.config.maxFiles) {
                                ng.$apply(function() {
                                    fn.renderShowMessage(true);
                                    ng.showMessage = 'Sólo se puede adjuntar ' + ng.config.maxFiles + ' archivo(s)';
                                });
                                return false;
                            } else if (file.size > ng.config.maxSize) {
                                ng.$apply(function() {
                                    fn.renderShowMessage(true);
                                    ng.showMessage = 'El tamaño del archivo supera: ' + fn.toBytes(ng.config.maxSize);
                                });
                                return false;
                            } else if (!fn.validateExtension(file.name)) {
                                ng.$apply(function() {
                                    fn.renderShowMessage(true);
                                    ng.showMessage = 'El tipo de archivo no está permitido';
                                });
                                return false;
                            } else {
                                var hash = 'ID_' + new Date().getTime() + '_' + fn.createHash(file.name);
                                var val = {};
                                val.status = {
                                    isCancel: true,
                                    isError: false,
                                    isDone: false
                                };
                                val.file = file;
                                data.hashKey = val.hashKey = hash;
                                val.jqXHR = data.submit();
                                ng.queue.push(val);
                                ng.$apply();
                            }
                        } else {
                            return false;
                        }
                    },
                    submit: function(e, data) {
                        if (typeof ng.config.formData !== 'undefined') {
                            data.formData = {};
                            angular.extend(data.formData, ng.config.formData);
                        }
                    },
                    done: function(e, data) {
                        if (typeof data.result.files !== 'undefined' && data.result.files.length > 0) {
                            var file = data.result.files[0];
                            var pos = fn.getElementArray(data.hashKey);
                            if (pos !== -1) {
                                ng.queue[pos].status = {
                                    isCancel: file.isCancel,
                                    isError: file.isError,
                                    isDone: file.isDone
                                };
                                ng.queue[pos].msg = file.msg;
                                ng.queue[pos].file = file;
                                if (file.isDone) {
                                    ng.$emit('FILE_UPLOAD_DONE', { file: angular.copy(file), queue: angular.copy(ng.queue.length), key : ng.config.key });
                                }
                                ng.$apply();
                            }
                        }
                    },
                    fail: function(e, data) {
                        if (data.textStatus !== 'abort') {
                            var pos = fn.getElementArray(data.hashKey);
                            if (pos !== -1) {
                                ng.queue[pos].status = {
                                    isCancel: false,
                                    isError: true,
                                    isDone: false
                                };
                                ng.queue[pos].msg = data.textStatus;
                                ng.$apply();
                            }
                        }
                    }
                });

                ng.disabled = true;

                ng.$watch('render', function(newValue, oldValue) {
                    if (typeof newValue !== 'undefined' && newValue) {
                        fn.render();
                        ng.render = false;
                    }
                });

                ng.$watch('update', function(newValue, oldValue) {
                    if (typeof newValue !== 'undefined' && newValue) {
                        fn.update();
                        ng.update = false;
                    }
                });

            },
            templateUrl: config.APP + 'template/directives/fileupload.html'
        };
    });

    module.directive('uiSwitch', function() {
        return {
            restrict: 'EA',
            replace: true,
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl) {
              $(element).switchButton({
                  on_label: 'Eng.',
                  off_label: 'Esp.',
                  width: 40,
                    height: 20,
                  button_width: 20
                });
                $(element).change(function () {
                    ctrl.$setViewValue($(element).is(":checked"));
                });
                ctrl.$formatters.unshift(function(viewValue) {
                    $(element).switchButton("option", "checked", viewValue)
                    return viewValue;
                });

            }
        };
    });

});