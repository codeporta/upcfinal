'use strict';

define(['config'], function (config) {
	var module = angular.module(config.NAMESPACE + '.directives',[]);

  module.directive('upcChecklist', function () {
    return {
      restrict: 'E',
      templateUrl: 'application/views/checklist.html',
      scope: {
        applicant: '='
      },
      controller: ['$scope', '$http','ngDialog','$sce','$timeout', 'Upload', 
                    function ($scope, $http, ngDialog, $sce, $timeout, Upload) {

        var ng = $scope;

        ng.models = (function () {
          return {
            tasks: []
          }
        })();

        ng.onClick = (function () {
          return {
            showFile: function (item) {
              ng.pdf = $sce.trustAsResourceUrl(item.task.template_file);
              ng.showLoadingIframe = true;
              ngDialog.open({ template: 'templateIframeId' , closeByDocument :false, width: '80%', height: 600, scope: ng});
              $timeout(function(){
                ng.showLoadingIframe = false;
              },750);
            },
            uploadFile: function(file, task) {

              task.error = ''
              if (!file) return;

              if (file.size > config.CONSTANTS.MAX_FILE_SIZE_BYTES) {
                task.error = 'El archivo excede el tamaño máximo de ' + config.CONSTANTS.MAX_FILE_SIZE_LABEL;
                return;
              }

              ng.loadingChecklist = true;
              ng.readyChecklist = false;
              Upload.upload({
                method: 'POST',
                url: 'checklist/' + task.id + '/resolve/file/',
                data: {file: file}
              })
              .then(function (response) {
                if (response.data) {
                  api.getChecklist();
                }
              }, function (error) {
                ng.loadingChecklist = false;
                ng.readyChecklist = true;
                // TO DO: Notificar error
              }, function (evt) {
                // TO DO: Puedes mostrar el progreso aqui
              });
            }
          }
        })();

        var api = (function(){
          return {
            getChecklist : function () {
              ng.loadingChecklist = true;
              ng.readyChecklist = false;
              $http.get('checklist/').then(function(response){
                angular.forEach(response.data,function(val,key){
                  var css = '';
                  switch(val.status.id){
                    case config.CONSTANTS.TASK_STATUS.PENDING:
                      css = 'check-list-item-pending';
                    break;
                    case config.CONSTANTS.TASK_STATUS.APPROVED:
                      css = 'check-list-item-approved';
                    break;
                    case config.CONSTANTS.TASK_STATUS.REVISION:
                      css = 'check-list-item-revision';
                    case config.CONSTANTS.TASK_STATUS.OBSERVED:
                      css = 'check-list-item-revision';
                    break;
                    case config.CONSTANTS.TASK_STATUS.INACTIVE:
                      css = 'check-list-item-inactive';
                    break;
                  }

                  if (val.task.code === config.CONSTANTS.TASKS.PAYMENT_VOUCHER) {
                    val.isPayment = true;
                  } else if (val.task.solution_type === config.CONSTANTS.SOLUTION_TYPES.FILE_UPLOAD) {
                    val.isUpload = true;
                  }

                  if (val.status.id === config.CONSTANTS.TASK_STATUS.PENDING || 
                      val.status.id === config.CONSTANTS.TASK_STATUS.REVISION ||
                      val.status.id === config.CONSTANTS.TASK_STATUS.OBSERVED) {
                    css += " check-list-item-enabled";
                    val.current = true;
                  }

                  val.css = css;
                });
                ng.models.tasks = angular.copy(response.data);
                ng.loadingChecklist = false;
                ng.readyChecklist = true;
              },function(error){

              });
            }
          }
        })();


        api.getChecklist();

      }]
    };
  })

	return module;
});