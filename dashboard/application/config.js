'use strict'

define([], function(){

	var config = {
		NAMESPACE : 'upc.codeporta',
		APP : 'application/',
		URL_LOGIN : 'http://upc.doous/#/login',
		URL_BASE : 'http://upc.doous/dashboard/',
		URL_API : 'http://admision2.eastus2.cloudapp.azure.com/api/applicant/',
		CONSTANTS : {
			STUDENT : {
				PREFERENTE : {
					id : 1,
					description : 'preferente'
				},
				REGULAR : {
					id : 2,
					description : 'regular'
				}
			},
			TASK_STATUS: {
				PENDING: 13,
				REVISION: 14,
				APPROVED: 15,
				OBSERVED: 16,
				INACTIVE: 17
			},
			TASKS: {
				PAYMENT_VOUCHER: 2
			},
			SOLUTION_TYPES: {
				INTEGRAL_EVALUATION: 1,
				FILE_UPLOAD: 2,
				FORM: 3
			},
			MAX_FILE_SIZE_BYTES: 10 * 1024 * 1024, // 10 MB,
			MAX_FILE_SIZE_LABEL: '10MB'
		}
	};

	return config;
});