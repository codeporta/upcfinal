'use strict';

define(['config', 'angular-route', 'angular-cookies' , 'angular-animate', 'angular-resource', 'controllers/controllers', 'modules/routeResolver'], function (config) {

  var app = angular.module('appModule', ['ngFileUpload', 'ngCookies','ngRoute', 'ngAnimate', 'ngResource', 'routeResolverServices', config.NAMESPACE + '.controllers']);

  app.constant('constants', {

  });
  app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$httpProvider', '$provide',
    function ($routeProvider, routeResolverProvider, $controllerProvider, $compileProvider, $filterProvider, $httpProvider, $provide) {

      $compileProvider.debugInfoEnabled(false);

      app.register =
        {
          controller: $controllerProvider.register,
          directive: $compileProvider.directive,
          filter: $filterProvider.register,
          factory: $provide.factory,
          service: $provide.service
        };

      $httpProvider.defaults.headers.post['Accept'] = 'application/json';
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';

     $httpProvider.interceptors.push(function($q, $filter, $cookies) {
        return {
          'request': function(conf) {
            if(conf.url.search('.html')===-1 && conf.url.search('templateIframeId')===-1){
              conf.url = config.URL_API + conf.url;
              conf.headers.Authorization = 'Token '+ $cookies.get('TOKEN');             
            }
            return conf;
          },
          'responseError': function(rejection) {
            if(rejection.status === 401) {
              console.log('No autorizado');
              window.location.href=config.URL_LOGIN;
            }
            return $q.reject(rejection);
          }
        };
      });

      var route = routeResolverProvider.route;
      /* if(APP.DATA.CONFIG.ALIAS_ROUTES_LIST.length > 0){
         $.each(APP.DATA.CONFIG.ALIAS_ROUTES_LIST, function (key, val) {
           var aliashash= val.split('/');
           var aliasRoute = '';
           for (var i = 0; i < aliashash.length; i++) {
             aliasRoute += '/' + aliashash[i];
           }
           $routeProvider.when(aliasRoute, route.resolve(aliashash[0]));
           //console.log("route: ", aliasRoute);
         });
       }*/
      $routeProvider.when('/404', route.resolve('404'));
      $routeProvider.when('/momento2', route.resolve('momento2'));
      $routeProvider.when('/momento3', route.resolve('momento3'));
      $routeProvider.when('/', { redirectTo: '/momento2' });
      $routeProvider.otherwise({ redirectTo: '/404' });

    }]);


  app.controller('appController', function ($scope, $rootScope, $location, $http, globalService) {

    var ng = $scope;

    ng.language = true;

    ng.models = (function(){
      return {
        user : null,
        applicant : null
      };
    })();

    var rest = (function(){
      return {
        getuser : function(){
          $http.get('user/').then(function(response){
            ng.models.user = angular.copy(response.data);
            ng.language = (ng.models.user.language==='es') ? false : true;
          },function(error){

          });
        },
        get : function(){
          $http.get('').then(function(response){
            ng.models.applicant = angular.copy(response.data); 
            globalService.applicant = response.data;
          },function(error){
            
          });
        },
        update : function(){
          var data = {
            phone_home : ng.models.applicant.phone_home
          };
          ng.savePhone = true;
          $http.patch('', data).then(function(response){
            ng.models.applicant = response.data;
            ng.savePhone = false;
          },function(error){
            
          });
        }
      };
    })();

    ng.onKey = (function(){
      return {
        enter : function(keyEvent){
          if(keyEvent.which === 13){
            rest.update();
          }
        }
      };
    })();


    $rootScope.$on('$routeChangeStart', function (event, next, current) {
      //console.log('change: ', event);
    });

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
      //console.log('completo: ', event);
      APP.DATA.FN.removeLoadingPage();


      setTimeout(function () {

        $(window).trigger('resize');

        $(document).trigger('GALLERY_RENDER');
        
      }, 1500);




    });

    $rootScope.$on("$routeChangeError", function (event) {
      console.log('error: ', event);
      $location.path('/404');
    });

    (function(){
      rest.getuser();
      rest.get();
    })();

  });

  return app;
});
