var APP = APP || {};
APP.DATA = APP.DATA || {};
APP.DATA.FN = APP.DATA.FN || {};
//Mostrar mensaje de inicio de la aplicación
APP.DATA.FN.showMessageStart = function(code,msg){
    var message = '';
    switch(code){
        case 'CODE001':
        message= 'Iniciando el sistema...';
        break;
        case 'CODE002':
        message = 'Obteniendo acceso...';
        break;
        case 'CODE003':
        message = 'Ocurrió un error, presione la tecla F5';
        break;
        case 'CODE004':
        message = 'Su navegador '+platform.name+' '+platform.version+' está obsoleto, tiene que actualizarlo.'; 
        break;       
        default:
        message = msg;
        break;
    }
    $("#loading-msg").text(message);
};
//Eliminar mensaje de inicio de la aplicación
APP.DATA.FN.removeMessageStart=function(id){
    setTimeout(function(){
        $('#loading').remove();
        $('#loading-mask').fadeOut('fast', function () {
            $('#' + (typeof id!=='undefined' ? id : 'main')).fadeIn('fast');
            $('#loading-mask').remove();
        });
    },1000);
};
//Agregar icono loading
APP.DATA.FN.showLoadingPage = function(){
	$('body').prepend('<div class="addLoadingPage" style="background: #fff url('+APP.DATA.CONFIG.URL_BASE+'/static/img/loading.gif) 50% 50% no-repeat;height: 100%;position: fixed;left: 0;top: 0;width: 100%;z-index: 20000;opacity: 0.90;"></div>');
};
//remover icono loading
APP.DATA.FN.removeLoadingPage = function(fn, id){
	setTimeout(function(){
        $('body>.addLoadingPage').remove();
        $('#' + (typeof id!=='undefined' ? id : 'main')).show();
        if(typeof fn !=='undefined'){
            fn();
        }
    },500); 
};
//deshabilitar el regreso hacia la página anterior al presionar la tecla backspace.
APP.DATA.FN.disabledBackspace = function(){
	$(document).keydown(function(e) {
        //var elid = $(document.activeElement).hasClass('textInput');
        var elid = $(document.activeElement).attr('data-ng-model') || $(document.activeElement).attr('ng-model');
        if (e.keyCode === 8 && !elid) {
            return false;
        };
    });
};
//mostrar un elemento
APP.DATA.FN.show = function(element,isClass){
    if(typeof isClass!=='undefined' && isClass){
        $('.'+element).show();
    }else{
        $('#'+element).show();
    }
};

APP.DATA.FN.isUndefined= function(value){
    return typeof value === 'undefined';
}       

APP.DATA.FN.isEmpty= function(value) {
    return APP.DATA.FN.isUndefined(value) || value === '' || value === null || value !== value;
}

/**
 *     @desc: Convert string to it boolean representation
 *     @type: private
 *     @param: inputString - string for covertion
 *     @topic: 0
 */
 APP.DATA.FN.convertStringToBoolean = function(inputString){
    if (typeof (inputString) == "string")
        inputString=inputString.toLowerCase();

    switch (inputString){
        case "1":
        case "true":
        case "yes":
        case "y":
        case 1:
        case true:
        return true;
        break;

        default: return false;
    }
}

//verificar navegador web
APP.DATA.FN.isValidNavigator = function(){   
    if(platform.name === 'IE'){
        var version = platform.version.split('.');
        version = parseInt(version[0],10);
        if(version < 9){
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
};

//poblar los arrays de los alias y los enlaces de la aplicación
APP.DATA.FN.setRoutes = function(menus, alias){
    APP.DATA.CONFIG.ALIAS_ROUTES_LIST = alias;
    APP.DATA.CONFIG.ROUTES_LIST = menus;
};

//funcionalidades al cargar la página por JQuery
APP.DATA.FN.onReady = function(){
    (function(){
        
        $(window).on('resize', function () {

          //console.log('ON RESIZE');

            var height = $(window).height();
            var width = $(window).width();
            var header = $('.header').outerHeight(true);
            //console.log('height,width,header: ',height,width,header);            
            var x = null;
          
          (function(){

            var checkListHead = $('.check-list-head').outerHeight(true);
            $('#check-list-content').height(height - header - checkListHead);
            if(!$('#check-list-content').hasClass('create-scroll')){
                $('#check-list-content').addClass('create-scroll');
                $('#check-list-content').enscroll({
                  showOnHover: true,
                  verticalScrolling: true,
                  verticalTrackClass: 'track3',
                  verticalHandleClass: 'handle3'
                });
            }
            
            if(width<=1000){
                if($('#scroll-1').hasClass('create-scroll')){
                    $( '#scroll-1' ).removeClass('create-scroll');
                    $( '#scroll-1' ).enscroll( 'destroy' );
                    $( '#scroll-1' ).removeAttr('style');
                }
                if($('#scroll-2').hasClass('create-scroll')){
                    $( '#scroll-2' ).removeClass('create-scroll');
                    $( '#scroll-2' ).enscroll( 'destroy' );
                    $( '#scroll-2' ).removeAttr('style');
                }
            }else{
                //console.log('mayor a 1000');
                var h = height - header - $('.left-bar-top').outerHeight(true) - 50 - 35 - 10;
                $('#scroll-1').height(h);
                $('#scroll-2').height(h);

                if(!$('#scroll-1').hasClass('create-scroll')){
                    $('#scroll-1').addClass('create-scroll');
                    $('#scroll-1').enscroll({
                        showOnHover: true,
                        verticalScrolling: true,
                        verticalTrackClass: 'track3',
                        verticalHandleClass: 'handle3'
                      });
                }

                if(!$('#scroll-2').hasClass('create-scroll')){
                    $('#scroll-2').addClass('create-scroll');
                    x = $('#scroll-2').enscroll({
                        showOnHover: true,
                        verticalScrolling: true,
                        verticalTrackClass: 'track3',
                        verticalHandleClass: 'handle3'
                      });
                }
            }
            

          })();
          
        });

        (function(){
            

              $(document).on('click','.auth',function(e){
                e.stopPropagation();
                if($(this).hasClass('open-auth')){
                    $(this).removeClass('open-auth');                    
                    $('#auth-menu').stop().animate({top: '-550px'}, "slow",function(){
                       //$('.header').css('border-bottom','3px solid #000000'); 
                    });
                }else{
                    $(this).addClass('open-auth');
                    //$('.header').css('border-bottom','3px solid #fcfcfc');
                    $('#auth-menu').stop().animate({top: '79px'}, "slow");
                }                
              });
              $(document).on('click','#auth-menu',function(e){
                e.stopPropagation();
              });
              $(document).on('click',function(){
                    if($('.auth').hasClass('open-auth')){
                        $('.auth').removeClass('open-auth');
                        //$('.header').css('border-bottom','3px solid #000000');
                        $('#auth-menu').stop().animate({top: '-600px'}, "slow", function(){
                          // $('.header').css('border-bottom','3px solid #000000'); 
                        });
                    }
              });
              $(document).on('click','.close-auth',function(e){
                e.preventDefault();
                $(document).click();
              });

              

              $(document).on('GALLERY_RENDER',function(){
                   $("#lightgallery").lightGallery({
                    closable: false,
                    download: false,
                    scale: 0.5,
                    addClass: 'normal-size'
                  });

                  $('#video-gallery').lightGallery({
                    width: '700px',
                    height: '470px',
                    mode: 'lg-fade',
                    counter: false,
                    hideBarsDelay:1000*60*60,
                    closable: false,
                    download: false,
                    zoom: false,
                    addClass: 'fixed-size'
                  });
                  $('#video-gallery-2').lightGallery({
                    width: '700px',
                    height: '470px',
                    mode: 'lg-fade',
                    counter: false,
                    hideBarsDelay:1000*60*60,
                    closable: false,
                    download: false,
                    zoom: false,
                    addClass: 'fixed-size'
                  });
                  
              });

                
          })();

    })();
};